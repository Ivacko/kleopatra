<!--Aleksa Ivacko 2014/0119-->
<?php
	/**
	* Aleksandra Todorovic 0333/2104
	*/
	class Korisnik_model extends CI_Model{
		public function __construct(){
			$this->load->database();
		}
		public function getKorisnik($data){
			$this->db->where('Username', $data['name']);
			$this->db->where('Password', $data['pass']);
			$upit = $this->db->get('korisnik');

			if($upit->num_rows() == 0)
				return null;
			foreach($upit->result() as $row){
				return $row;
			}

		}
		public function getRegularan($id){
			$this->db->where('Id', $id);
			$upit = $this->db->get('regularan');
			foreach($upit->result() as $row){
				return $row;
			}
		}

		public function getModerator($id){
			$this->db->where('Id', $id);
			$upit = $this->db->get('moderator');
			foreach($upit->result() as $row){
				return $row;
			}
		}

		public function isBanned($id){
			$this->db->where('IdKor', $id);
			$upit = $this->db->get('zabrane');
			foreach($upit->result() as $row){
				if($row->DatumDo == null || $row->DatumDo > date('Y-m-d'))
					return true;
			}
			return false;
		}
		public function isUniqueUsername($user){
			// $this->db->where('Username', $user);
			$upit = $this->db->get_where('korisnik', array('Username' => $user));
			if ($upit->num_rows() > 0){
		        return false;
			}
		    else{
		        return true;
		    }
		}

		public function insertKorisnik($data){
			$dataK = array(
				'Username' => $data['user'],
				'Password' => $data['pass'],
				'Tip' => 'K'
				);
			$this->db->insert('korisnik', $dataK);
			$insert_id = $this->db->insert_id();
			if(isset($insert_id)==FALSE)
				return false;
			$dataR = array(
				'Id' => $insert_id,
				'Ime' => $data['name'],
				'Prezime' => $data['surname'],
				'DatumRodjenja' => $data['date'],
				'Email' => $data['email'],
				'Mesto' => $data['place'],
				'Pol' => $data['pol'],
				'Znak' => $data['znak']
				);
			$this->db->insert('regularan', $dataR);
			return ($this->db->affected_rows() != 1) ? false : true;
		}

		public function insertModerator($data){
			$dataK = array(
				'Username' => $data['user'],
				'Password' => $data['pass'],
				'Tip' => 'M'
				);
			$this->db->insert('korisnik', $dataK);
			$insert_id = $this->db->insert_id();
			if(isset($insert_id)==FALSE)
				return false;
			$dataM = array(
				'Id' => $insert_id,
				'Email' => $data['email'],
				'Opis' => $data['text']
				);
			$this->db->insert('moderator', $dataM);
			return ($this->db->affected_rows() != 1) ? false : true;
		}

		public function updateKorisnik($id, $data){
			$this->db->where('Id', $id);
			$this->db->update('regularan', $data); 
			return ($this->db->affected_rows() != 1) ? false : true;
		}

		public function izbrisiModeratora($id)
		{
			$this->db->where('Id', $id);
			$this->db->delete('korisnik');
			return true;
		}

		public function getModeratori($tip)
		{
			$i=0;
			$query=$this->db->get_where('moderator', array('Tip'=>'M', 'Status'=>$tip));
			;
			if($query->num_rows() == 0)
				return null;
			else{
				foreach ($query->result_array() as $row) 
			{
				
				$query1=$this->db->get_where('korisnik', array('Id'=>$row['Id']));
				foreach($query1->result_array() as $row1)
				{

					if ($row1['Id']===$row['Id'])
						$result[$i++]=array_merge($row,$row1);
				}
				
				
			}
			return $result;
			}
		}

		public function odobriModeratora()
		{
			
			$data = array(
	                'Id' => $this->input->post('Id'),
	                'Tip' => $this->input->post('Tip'),
	                'Email' => $this->input->post('Email'),
	                'BrZabrana' => $this->input->post('BrZabrana'),
	               
	                'BrKomentara' => $this->input->post('BrKomentara'),
	                'BrNapisanih' => $this->input->post('BrNapisanih'),
	                'BrRezervisanih' => $this->input->post('BrRezervisanih'),
	                 'Status' => 'O'

	            );
			$this->db->where('Id',$this->input->post('Id'));
	        $this->db->update('moderator', $data);

		}

		public function getPrijave()
		{
			$i=0;
			$query=$this->db->get_where('zabrane', array('DatumOd'=>null));
			$result;
			if($query->num_rows() == 0)
				return null;
			else
			{
				foreach ($query->result_array() as $row) 
				{
					
					$query1=$this->db->get_where('regularan', array('Id'=>$row['IdKor']));
					foreach($query1->result_array() as $row1)
					{

						if ($row1['Id']===$row['IdKor'])
							$result[$i++]=array_merge($row,$row1);
					}
				
				
				}
				return $result;
			}
		}

		public function izbrisiKorPrijava($id)
		{
			$this->db->where('IdKor', $id);
			$this->db->delete('zabrane');
			return true;
		}

		public function ban8()
		{
			
			$data = array(
	                //'Id' => $this->input->post('Id'),
	                'IdKor' => $this->input->post('IdKor'),
	                'IdMod' => $this->input->post('IdMod'),
	                'DatumOd' => date('Y-m-d H:i:s'),
	                'DatumDo' =>date('Y-m-d H:i:s A', time()+28800),
	                'Razlog'=>$this->input->post('Razlog')
	            );
			$this->db->where('IdKor',$this->input->post('IdKor'));
	        $this->db->update('zabrane', $data);

		}

		public function ban24()
		{
			
			$data = array(
	               // 'Id' => $this->input->post('Id'),
	                'IdKor' => $this->input->post('IdKor'),
	                'IdMod' => $this->input->post('IdMod'),
					'DatumOd' => date('Y-m-d H:i:s'),
	                'DatumDo' =>date('Y-m-d H:i:s A', time()+86400),
	                'Razlog'=>$this->input->post('Razlog')

	            );
			$this->db->where('IdKor',$this->input->post('IdKor'));
	        $this->db->update('zabrane', $data);

		}
		public function ban72()
		{
			
			$data = array(
	               // 'Id' => $this->input->post('Id'),
	                'IdKor' => $this->input->post('IdKor'),
	                'IdMod' => $this->input->post('IdMod'),
	                'DatumOd' => date('Y-m-d H:i:s'),
	                'DatumDo' =>date('Y-m-d H:i:s A', time()+259200),
	                'Razlog'=>$this->input->post('Razlog')

	            );
			$this->db->where('IdKor',$this->input->post('IdKor'));
	        $this->db->update('zabrane', $data);

		}
		public function ban()
		{
			
			$data = array(
	                //'Id' => $this->input->post('Id'),
	                'IdKor' => $this->input->post('IdKor'),
	                'IdMod' => $this->input->post('IdMod'),
	                'DatumOd' => date('Y-m-d'),
	                'DatumDo' =>'null',
	                'Razlog'=>$this->input->post('Razlog')

	            );
			$this->db->where('IdKor',$this->input->post('IdKor'));
	        $this->db->update('zabrane', $data);

		}

		public function getPrijave2()
		{
			$i=0;
			$sql="SELECT * FROM zabrane WHERE DatumOd IS NOT NULL";
			$query=$this->db->query($sql);
			$result;
			if($query->num_rows() == 0)
				return null;
			else
			{
				foreach ($query->result_array() as $row) 
				{
					
					$query1=$this->db->get_where('regularan', array('Id'=>$row['IdKor']));
					foreach($query1->result_array() as $row1)
					{

						if ($row1['Id']===$row['IdKor'])
							$result[$i++]=array_merge($row,$row1);
					}
				
				
				}
				return $result;
			}
		}

		
	}