<?php /*Tanja Zivkovic 14/0469*/
	
	class Clanak_model extends CI_Model{
		public function __construct(){
			$this->load->database();
		}

		public function dohvati_clanke(){
			$this->db->order_by('Id', 'DESC');
			$query=$this->db->get_where('clanak', array("Tip"=>'M'));
			return $query->result_array();
		}
						
		public function dohvati_zadnji($znak){
			$this->db->select('*');
			$this->db->from('clanak');
			$this->db->join('mesecni', 'clanak.Id = mesecni.Id', 'left');
			$this->db->where(array("clanak.Tip"=>'M', 'clanak.Znak'=>$znak, 'DatumOd <='=>date('Y-m-d')));
			$this->db->order_by('DatumOd', 'DESC');
			$query = $this->db->get();
			//print_r($query->row_array());
			return $query->row_array();
		}

		public function dohvati_osobine($znak){
			$this->db->select('*');
			$this->db->from('clanak');
			$this->db->where(array("clanak.Tip"=>'O', 'clanak.Znak'=>$znak));
			$query = $this->db->get();
			//print_r($query->row_array());
			return $query->row_array();
		}

		public function dohvati_slaganje($znak){
			$this->db->select('*');
			$this->db->from('clanak');
			$this->db->join('slaganje', 'clanak.Id = slaganje.Id', 'left');
			$this->db->where(array("clanak.Tip"=>'S', 'slaganje.ZnakOd'=>$znak));
			$query = $this->db->get();
			// print_r("ovo dohvata za slaganje");
			// print_r($query->result_array());
			// print_r("ovo dohvata za slaganje");
			return $query->result_array();
		}

		public function dohvati_komentare($id){
			//print_r($id);
			$this->db->order_by('Datum', 'DESC');
			// $this->db->limit(10);
			$query=$this->db->get_where('komentar', array('IdClan'=>$id, 'Status'=>'O'));
			//print_r($query->result_array());
			if ($query->result_array()!=null)
			return $query->result_array();
			else return null;
		}

		public function ubaci_komentar($data){
			$datat['IdAutor']=$data['IdAutor'];
			$datat['Datum']=date('Y-m-d');
			$datat['Tekst']=$data['Tekst'];
			//$datat['Tip']='C';
			$datat['IdClan']=$data['IdClan'];
			//print_r($datat);
			$this->db->insert('komentar', $datat);
		}

		public function ubaci_clanak($data){
			//print_r($data);
			$this->db->insert('clanak',$data);
			$insert_id = $this->db->insert_id();
			//print_r($insert_id);
			return $insert_id;
		}

		public function izmeni_clanak($data){
			//print_r($data);
			$this->db->where('Id', $data['Id']);
			$this->db->update('clanak',$data);
		}

		public function ubaci_mesecni($data){
			//print_r($data);
			$this->db->insert('mesecni', $data);
		}
		public function izmeni_mesecni($data){
			//print_r($data);
			$this->db->where('Id', $data['Id']);
			$this->db->update('mesecni', $data);
		}

		public function ubaci_autora($data){
			$this->db->insert('autori', $data);
		}


		public function dohvati_clanak($id){
			$this->db->select('*');
			$query=$this->db->get_where('mesecni', array('Id'=>$id));
			//print_r($query->row_array());
			return $query->row_array();
		}

		public function ubaci_slaganje($data){
			//print_r($data);
			$this->db->insert('slaganje', $data);
		}

		public function dohvati_moje($id){
			$this->db->select('*');
			$this->db->from('autori');
			$this->db->join('clanak', 'autori.IdClan=clanak.Id', 'left');
			$this->db->where('autori.IdMod', $id);
			$query=$this->db->get();
			return $query->result_array();


		}
		public function dohvati_jedan($id, $tip){
			$this->db->select('*');
			$this->db->from('clanak');
			switch($tip){
				case 'M':$this->db->join('mesecni', 'clanak.Id=mesecni.Id','left');
				$query=$this->db->where('mesecni.Id', $id);
				break;
				case 'S':$this->db->join('slaganje', 'clanak.Id=slaganje.Id', 'left');
				$query=$this->db->where('slaganje.Id', $id);
				break;
				case 'O':
				$this->db->where('Id', $id);
				break;
			}
			$query=$this->db->get();
			return $query->row_array();
			
		}
		public function obrisi($id){
			$this->db->where('Id', $id);
			$this->db->delete('clanak');
		}

	}

?>