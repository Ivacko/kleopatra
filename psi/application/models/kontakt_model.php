
<?php
	/**
	* Aleksandra Todorovic 0333/2104
	*/
	class kontakt_model extends CI_Model
	{
		public function __construct()
		{
			$this->load->database();
		}

		public function getKontakti()
		{
			$pol=$_SESSION['pol'];
			if ($pol=='Z')
			{
				$i=0;
				$query=$this->db->get_where('kontakti', array('IdZ'=>$_SESSION['id']));
				$result;
				if($query->num_rows() == 0)
					return null;
				else
				{
					foreach ($query->result_array() as $row) 
					{
						
						$query1=$this->db->get_where('regularan', array('Id'=>$row['IdM']));
						foreach($query1->result_array() as $row1)
						{

							if ($row1['Id']===$row['IdM'])
								$result[$i++]=array_merge($row,$row1);
						}
					
					
					}
					return $result;
				}

			}
			elseif ($pol=='M')
			{
				$i=0;
				$query=$this->db->get_where('kontakti', array('IdM'=>$_SESSION['id']));
				$result;
				if($query->num_rows() == 0)
					return null;
				else
				{
					foreach ($query->result_array() as $row) 
					{
						
						$query1=$this->db->get_where('regularan', array('Id'=>$row['IdZ']));
						foreach($query1->result_array() as $row1)
						{

							if ($row1['Id']===$row['IdZ'])
								$result[$i++]=array_merge($row,$row1);
						}
					
					
					}
					return $result;
				}


			}
		}


		public function blokiraj($id, $pol)
		{
			if ($pol=='Z')
			{
				$data = array(
	                'IdOd' => $_SESSION['id'],
	                'IdZa' => $this->input->post('IdM'),
	                'Ime' => $this->input->post('Ime'),
	                'Prezime' => $this->input->post('Prezime'),
	                'DatumOd' =>date('Y-m-d'),

	            );
	            print_r($data);

	            
				$this->db->delete('kontakti', array('IdM' =>$this->input->post('IdM'),'IdZ' => $this->input->post('IdZ') )); 
			}
			elseif ($pol=='M')
			{
				$data = array(
	                'IdOd' => $_SESSION['id'],
	                'IdZa' => $this->input->post('IdZ'),
	               // 'Ime' => $this->input->post('Ime'),
	              //  'Prezime' => $this->input->post('Prezime'),
	                'DatumOd' =>date('Y-m-d'),

	            );
	           $this->db->insert('blokirani',$data); 
	           $this->db->delete('kontakti', array('IdM' =>$this->input->post('IdM'),'IdZ' => $this->input->post('IdZ') )); 
			}
		}

		public function prijavi($razlog, $pol)
		{
			$maxid = 0;
			$row = $this->db->query('SELECT MAX(id) AS `Id` FROM `zabrane`')->row_array();
			if ($row) {
			    $maxid = $row['Id']; 
			}
			$maxid++;
			if ($pol=='Z')
			{
				$data = array(
					'Id'=>$maxid,
	                'IdKor' =>$this->input->post('IdM'),
	                'IdMod' => null,
	                'Razlog' => $razlog,
	                'DatumOd' =>null,
	                'DatumDo' =>null

	            );
	            
	            
				
	            
	            $this->db->insert('zabrane',$data);
			}
			elseif ($pol=='M')
			{
				$data = array(
					'Id'=>$maxid,
	                'IdKor' =>$this->input->post('IdZ'),
	                'IdMod' => null,
	                'Razlog' => $razlog,
	                'DatumOd' =>null,
	                'DatumDo' =>null

	            );
	            
	            $this->db->insert('zabrane',$data);
			}

		}


		public function getBlokirani()
		{


			$query=$this->db->get_where('blokirani', array('IdOd'=>$_SESSION['id']));
			$result;
			$i=0;
			if($query->num_rows() == 0)
				return null;
			else
			{
				foreach ($query->result_array() as $row) 
				{
					$result[$i++]=$row;
				
				}
				return $result;
			}


		}

		public function odblokiraj($idza, $idod)
		{
			$this->db->delete('blokirani', array('IdOd'=>$this->input->post('IdOd'),'IdZa' => $this->input->post('IdZa') )); 

		}
}