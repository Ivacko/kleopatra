<?php
	/**
	* Aleksandra Todorovic 0333/2104
	*/
class Komentari_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}


	public function getNeobjavljeni($tip=FALSE)
	{
		if ($tip===FALSE)
		{
			$query=$this->db->get('komentar');
			return $query->result_array();
		}
		$i=0;
		$query=$this->db->get_where('komentar', array('Status'=>$tip));
		if($query->num_rows() == 0)
			return null;
		else{
			foreach ($query->result_array() as $row) 
		{
			$result[$i++]=$row;
		}
		return $result;
		}
		
	}

	public function izbrisiKomentar($id)
	{
		$this->db->where('Id', $id);
		$this->db->delete('komentar');
		return true;
	}
	
	public function objaviKomentar()
	{
		
		$data = array(
                'Id' => $this->input->post('Id'),
                'IdAutor' => $this->input->post('IdAutor'),
                'IdMod' => $this->input->post('IdMod'),
                'Datum' => $this->input->post('Datum'),
                'Status' => 'O',
                //'Tip' => $this->input->post('Tip'),
                'IdClan' => $this->input->post('IdClan'),
               // 'IdKor' => $this->input->post('IdKor'),
                'Ime' => $this->input->post('Ime'),
                'Prezime' => $this->input->post('Prezime')

            );
		$this->db->where('Id',$this->input->post('Id'));
        $this->db->update('komentar', $data);

	}
}
?>