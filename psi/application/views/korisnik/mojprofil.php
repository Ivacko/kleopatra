<!--Aleksa Ivacko 2014/0119-->
<!-- general -->
<div class="container">
	<div class="col-md-8 wthree-top-news-left" style="width: inherit;" >	
				<!-- agile-comments -->
		<div class="agile-news-comments-info" >
		    <h1>Edit Profile</h1>
		  	<hr>
			<div class="row">
		    	<!-- left column -->
		      	<div class="col-md-3">
			        <div class="text-center">
			         	<img src="<?php echo slike?>/user.jpg" class="avatar img-circle" alt="avatar" style="width: 200px; height: 200px">
			          	<h6>Upload a different photo...</h6>
			          
			          	<input type="file" class="form-control">
			        </div>
		    	</div>
		      
			      <!-- edit form column -->
			    <div class="col-md-9 personal-info">
			        <br/>
			        <h3 align="center">Personal info</h3>
			        <br/>
			        <form class="form-horizontal" role="form" method="post" action="<?php echo base_url()?>korisnici/azuriraj">

			            <div class="form-group">
			            	<label class="col-lg-3 control-label">Ime:</label>
			           		<div class="col-lg-8">
			              		<input class="form-control" name="name" type="text" value="<?php echo $ime?>">
			            	</div>
			          	</div>

			          	<div class="form-group">
			            	<label class="col-lg-3 control-label">Prezime:</label>
			            	<div class="col-lg-8">
			              		<input class="form-control" name="surname" type="text" value="<?php echo $prezime?>">
			            	</div>
			          	</div>

			          	<div class="form-group">
			            	<label class="col-lg-3 control-label">Mesto:</label>
			            	<div class="col-lg-8">
			              		<input class="form-control" name="place" type="text" value="<?php echo $mesto?>">
			            	</div>
			          	</div>

			            <div class="form-group">
			            	<label class="col-lg-3 control-label">Email:</label>
			            	<div class="col-lg-8">
			              		<input class="form-control" name="email" type="email" value="<?php echo $email?>">
			            	</div>
			          	</div>
			          
			          	<!-- <div class="form-group">
			            	<label class="col-md-3 control-label">Password:</label>
			            	<div class="col-md-8">
			              		<input class="form-control" type="password" value="11111122333">
			            	</div>
			            </div>

			          	<div class="form-group">
			            	<label class="col-md-3 control-label">Confirm password:</label>
			            	<div class="col-md-8">
			              		<input class="form-control" type="password" value="11111122333">
			            	</div>
			          	</div> -->

			          	<div class="form-group">
			            	<label class="col-md-3 control-label">Biografija:</label>
			            	<div class="col-md-8">
			              		<textarea class="form-control" name="bio" type="" value="<?php echo $biografija?>"><?php echo $biografija?></textarea> 
			            	</div>
			          	</div>

			          	<div class="form-group">
			            	<label class="col-md-3 control-label">Interesovanja:</label>
			            	<div class="col-md-8">
			              		<textarea class="form-control" name="interes" type="" value="<?php echo $zanimanja?>"><?php echo $zanimanja?></textarea> 
			            	</div>
			          	</div>

			          	<div class="form-group">
			            	<label class="col-md-3 control-label"></label>
		            		<div class="col-md-8">
		              			<input type="submit" class="login loginmodal-submit" value="Save Changes">
		              			<!-- <span></span>
		              			<input type="reset" class="login loginmodal-submit" value="Cancel"> -->
		            		</div>
			          	</div>
			        </form>
			    </div>
			</div>
			</hr>
		</div>
	</div>
</div>	

<!-- //general