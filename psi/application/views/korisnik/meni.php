<!-- Ana Đurić -->
<!-- nav -->
	<div class="movies_nav" id="menikorisnik">
		<div class="container">
			<nav class="navbar navbar-default">
				
				<div class="navbar-header navbar-left" >
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-2" >
					<nav>
						<ul class="nav navbar-nav">
							<li><a href="<?php echo base_url()?>korisnici/mojprofil"> Moj profil<br/>&nbsp;</a></li>
							<li><a href="<?php echo base_url()?>pretraga/index"> Nadji <br/>partnera</a></li>
							<li><a href="<?php echo base_url()?>korisnik/kontakti"> Moji <br/>kontakti</a></li>
							<li><a href="<?php echo base_url()?>korisnik/blokirani"> Blokirani<br/>korisnici</a></li>
							<li><a href="<?php echo base_url()?>pretraga/zahtev"> Obavestenja<br/>&nbsp;</a></li>
							
						</ul>
					</nav>
				</div>


			</nav>	
		</div>
	</div>
	
<!-- //nav -->