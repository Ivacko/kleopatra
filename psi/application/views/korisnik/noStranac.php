<!-- Ana Đurić -->
<!-- general -->

<div class="container">
	<div class="col-md-8 wthree-top-news-left" style="width: inherit;" >	
				<!-- agile-comments -->
		<div class="agile-news-comments-info" >
		    <h1>Profil</h1>
		  	<hr>
			<div class="row">
		    	<!-- left column -->
		      	<div class="col-md-3">
			        <div class="text-center">
			         	<img src="<?php echo slike?>/user.jpg" class="avatar img-circle" alt="avatar" style="width: 200px; height: 200px">
			          	
			          
			          	
			        </div>
		    	</div>
		      
			      <!-- edit form column -->
			    <div class="col-md-9 personal-info">
			        <br/>
			        <h3 align="center">Personal info</h3>
			        <br/>
			        <form class="form-horizontal" role="form">

			            <div class="form-group">
			            	<label class="col-lg-3 control-label">First name:</label> 
			           		<div class="col-lg-8">
			              		<?php echo $niz['Ime']; ?>
			            	</div>
			          	</div>

			          	<div class="form-group">
			            	<label class="col-lg-3 control-label">Last name:</label>
			            	<div class="col-lg-8">
			              		<?php echo $niz['Prezime']; ?>
			            	</div>
			          	</div>

			          	<div class="form-group">
			            	<label class="col-lg-3 control-label">Mesto:</label>
			            	<div class="col-lg-8">
			              		<?php echo $niz['Mesto']; ?>
			            	</div>
			          	</div>

			          	<div class="form-group">
			            	<label class="col-lg-3 control-label">Pol:</label>
			            	<div class="col-lg-8">
			              		<?php echo $niz['Pol']; ?>
			            	</div>
			          	</div>

			            

			            <div class="form-group" >
			            	<label class="col-lg-3 control-label">Email:</label>
			            	<div class="col-lg-8">
			              		<?php echo $niz['Email']; ?>
			            	</div>
			          	</div>
			          

			          	<div class="form-group">
			            	<label class="col-md-3 control-label">Biografija:</label>
			            	<div class="col-md-8">
			              		<?php echo $niz['Biografija']; ?>
			            	</div>
			          	</div>

			          	<div class="form-group">
			            	<label class="col-md-3 control-label">Zanimanja:</label>
			            	<div class="col-md-8">
			              		<?php echo $niz['Zanimanja']; ?>
			            	</div>
			          	</div>

			          	
			        </form>
			    </div>
			</div>
			</hr>
		</div>
	</div>
</div>	


<!-- //general -->