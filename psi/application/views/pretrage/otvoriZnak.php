<!-- Ana Đurić -->
<?php echo validation_errors(); ?>

<div class="container">
	<br/>
	<div class="col-md-8 wthree-top-news-left" style="width: inherit;" >	
				<!-- agile-comments -->
		<div class="agile-news-comments-info" >	
			<h4>Suzi izbor</h4>
			<br/>
			
			<?php echo form_open('pretraga/ucitajStranicu', $attributes = array('class' => 'form-horizontal', 'role' => 'form')); ?>
			<!--<form class="form-horizontal" role="form">-->
			<div class="form-group" style="width: 33%;">
				<div class="checkbox">
  					<label class="col-lg-3 control-label" ><input type="checkbox" name = "mesto" value=""> Mesto&nbsp;&nbsp;</label>
  					<div class="col-lg-8">
	              		<select class="form-control" id="sel1">
						    <option>Beograd</option>
						    <option>Loznica</option>
						    <option>Nis</option>
						    <option>Cacak</option>
						 </select>
            		</div>
				</div>
			</div>
			<div class="form-group" style="width: 33%;">
				<div class="checkbox" >
  					<label class="col-lg-3 control-label" ><input type="checkbox" name = "godine" value=""> Godine</label>
  					<div class="col-lg-8">
  						<p> Od:</p>
	              		<input type="text" style="width: 100px" name="">
	              		<p>   Do:</p>
	              		<input type="text" style="width: 100px" name="">
            		</div>
				</div>
			</div>
			<br/>
          	<div class="form-group" style="width: 33%;"> 
	          	<div class="checkbox">
	            	<label class="col-lg-3 control-label"><input type="checkbox" value="">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
	        		<div class="col-lg-8">
	        			<p> Broj znakova za prikaz</p>
	          			<input type="number" name = "number" style="width: 100px" min="1" max="12" >
	        		</div>
	          	</div>
          	</div>
			<br/>
          	<div class="form-group" style="width: 33%;"> 
            	<label class="col-md-3 control-label"></label>
        		<div class="col-md-8">
          			<a class="btn btn-default" href="<?php echo base_url()?>pretraga/ucitajStranicu"><b>Pretrazi</b></a>
        		</div>
          	</div>
          	
          	</form>
		</div>
	</div>
	<div class="clearfix"></div>
	<br/>