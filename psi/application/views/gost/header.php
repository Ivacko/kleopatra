<!--Aleksa Ivacko 2014/0119-->
<div class="header">
		<div class="container">
			<div class="row" >
				<div class="col-sm-4" >
					<div class="w3layouts_logo">
				        <a href="<?php echo base_url()?>gosti"><img style="height:80px; width:120px; float: left" src="<?php echo slike;?>logo.jpg"> </img><h1>Kleopatra<br/> Veličanstvena</h1></a>
			         </div>
				</div>
				<div class="col-sm-4">
					<center >
					</center>
				</div>
				<div class="col-sm-4" >
					<div class="w3l_sign_in_register" >
						<ul style="margin-right: 0px">

							<li id="login" ><button  data-toggle="modal" data-target="#login-modal" > login  </button> </li><br/>
								<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	    							<div class="modal-dialog">
										<div class="loginmodal-container">
											<h1>Login to Your Account</h1><br>
												<center><?php echo validation_errors(); ?></center>
					  							<?php echo form_open('gosti/login'); ?> 
										        <p>
										            <input type="text" id="username1" name="user" placeholder="Username" value="<?php echo set_value('user');?>">
													<input type="password" id="password1" name="pass" placeholder="Password">	
												</p>
										        <p><?php echo form_submit('submit', 'Potvrdi', 
										        " class=\"login loginmodal-submit\""); ?> </p>
										        <?php echo form_close(); ?>
										        
					  					</div>
									</div>
								</div>
		  

							<li id="register"  > <button href="" data-toggle="modal" data-target="#register-modal" > register</button> </li><br/>
								<div class="modal fade" id="register-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	    							<div class="modal-dialog">
										<div class="loginmodal-container">
											<h1>Register</h1><br>
												<center><?php echo validation_errors(); ?></center>
					  							<?php echo form_open('gosti/register'); ?> 
										        <p>
										            <input type="text" name="user" placeholder="Username" 
										            value="<?php echo set_value('user');?>">
													<input type="password" name="pass" placeholder="Password">
													<input type="password" name="passconf" placeholder="Confirm Password">
													<input type="email" name="email" placeholder="email" 
													value="<?php echo set_value('email');?>">
													<input type="text" name="name" placeholder="Name" 
													value="<?php echo set_value('name');?>">
													<input type="text" name="surname" placeholder="Surname" 
													value="<?php echo set_value('surname');?>">
													<input type="radio" name="gender" value="M"> Musko 
  													<input type="radio" name="gender" value="Z"> Zensko<br>
													<input type="date" name="date" placeholder="yyyy-mm-dd" 
													value="<?php echo set_value('date');?>">
													<input type="text" name="place" placeholder="Place of Birth" 
													value="<?php echo set_value('place');?>">
												</p>
										        <p><?php echo form_submit('register', 'Register', 
										        " class=\"login loginmodal-submit\""); ?> </p>
										        <?php echo form_close(); ?>
					  					</div>
									</div>
								</div>

							<li id="registermod"  > <button href="" data-toggle="modal" data-target="#registermod-modal" > postani moderator</button> </li><br/>
								<div class="modal fade" id="registermod-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	    							<div class="modal-dialog">
										<div class="loginmodal-container">
											<h1>Register Moderator</h1><br>
												<center><?php echo validation_errors(); ?></center>
					  							<?php echo form_open('gosti/registermod'); ?> 
										        <p>
										            <input type="text" name="user" placeholder="Username" 
										            value="<?php echo set_value('user');?>">
													<input type="password" name="pass" placeholder="Password">
													<input type="password" name="passconf" placeholder="Confirm Password">
													<input type="email" name="email" placeholder="email" 
													value="<?php echo set_value('email');?>">
													<br/>
													<label style="font-family: 'Roboto Condensed'">Opis</label>
													<textarea class="form-control" type="" name="text"></textarea>
													<br/>
												</p>
										        <p><?php echo form_submit('registerMod', 'Register Moderator', 
										        " class=\"login loginmodal-submit\""); ?> </p>
										        <?php echo form_close(); ?>
					  					</div>
									</div>
								</div>
												
					
						</ul>
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //header -->
