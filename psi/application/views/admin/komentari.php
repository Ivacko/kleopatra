<!-- Aleksandra Todorovic 0333/2014 -->
<div class="container">
	<div class="col-md-8 wthree-top-news-left" style="width: inherit;" >	
				<!-- agile-comments -->
		<div class="agile-news-comments-info" >	
			<table class="table">
			  <thead style="background-color: #FF8D1B;">
			    <tr >
			      <th style=" color:white;  ">#</th>
			      <th style=" color:white; ">Korisnik</th>
			      <th style=" color: white; ">Clanak</th>
			      <th style=" color: white;">Komentar</th>
			      <th style=" color: white;">Akcije</th>
			    </tr>
			  </thead>
			  <tbody >
			  	
			  	
			  
			  
			    <?php $num=1; ?>
			    <?php if (!($komentari==null)):?>
			    <?php foreach ($komentari as $komentar): ?>

			    <tr>
			    	<th scope="row"><?php echo $num++; ?> </th>
			    	<td> <?php echo $komentar['Ime'].' '. $komentar['Prezime'];?></td>
			    	<td> <?php echo $komentar['IdClan']?></td>
			    	<td> <?php echo $komentar['Tekst']?></td>
			    	<td>
				    	<?php echo form_open('admini/izbrisiKomentar/'.$komentar['Id'])?>
				    		<input type="submit" name="" value="Obrisi" class="login loginmodal-submit" style="padding:5px 10px;">
				    	</form>
				    	<span></span>
				    	<?php echo form_open('admini/objaviKomentar')?>
				    		<input type="submit" name="" value="Objavi" class="login loginmodal-submit" style="padding:5px 10px;">
				    		<input type="hidden" name="Id" value="<?php echo $komentar['Id'];?>">
				    		<input type="hidden" name="IdMod" value="<?php  echo  $_SESSION['id'];?>">
				    		<input type="hidden" name="IdAutor" value="<?php  echo $komentar['IdAutor'];?>">
				    		<input type="hidden" name="Datum" value="<?php echo  $komentar['Datum'];?>">
				    		<input type="hidden" name="Ime" value="<?php echo  $komentar['Ime'];?>">
				    		<input type="hidden" name="Prezime" value="<?php echo  $komentar['Prezime'];?>">
				    		<input type="hidden" name="IdClan" value="<?php echo  $komentar['IdClan'];?>">
				    		<input type="hidden" name="IdKor" value="<?php echo  $komentar['IdKor'];?>">
				    		<input type="hidden" name="Tip" value="<?php echo  $komentar['Tip'];?>">
				    	</form>
			    	
			        </td> 
			    	
			        
			    </tr>
			    <?php endforeach ?>
			    
			    <?php endif; ?>
			    
			  </tbody>
			</table>
		</div>
	</div>
</div>
