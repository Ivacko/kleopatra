<!-- Aleksandra Todorovic 0333/2014 -->
	<div class="movies_nav" id="menikorisnik">
		<div class="container">
			<nav class="navbar navbar-default">
				
				<div class="navbar-header navbar-left" >
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-2" >
					<nav>
						<ul class="nav navbar-nav">
							<li><a href="<?php echo base_url()?>Moderatori/moji_clanci"> Moji clanci<br/>&nbsp;</a></li>
							<li><a href="<?php echo base_url()?>admin/napisiclanak"> Napisi clanak<br/>&nbsp;</a></li>
							<li><a href="<?php echo base_url()?>admin/komentari"> Komentari<br/>&nbsp;</a></li>
							<li><a href="<?php echo base_url()?>admin/listamoderatora"> Lista<br/>moderatora</a></li>
							<li><a href="<?php echo base_url()?>admin/prijave"> Prijave <br/>korisnika</a></li>
							<li><a href="<?php echo base_url()?>admin/banovani"> Banovani<br/>korisnici</a></li>
							<li><a href="<?php echo base_url()?>admin/prijavemod"> Prijave za<br/>moderatora</a></li>
							
							
						</ul>
					</nav>
				</div>


			</nav>	
		</div>
	</div>
	
<!-- //nav -->