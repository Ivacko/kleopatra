<!-- general -->
	<div class="faq">
			<div class="container">
				<div class="agileits-news-top">
					<ol class="breadcrumb">
					  <li><a href="index.html">Pocetna</a></li>
					  <li class="active">Osobine znakova - ovan</li>
					</ol>
				</div>
				<div><h4 class="latest-text w3_latest_text" style="margin-left: 0px">Karakteristike ovna <br/><p style="font-size:1em"> (March 20 – April 19) </p> </h4></div>

				<img src="<?php echo slike;?>znak_ovan.jpg" class="img-responsive" alt="" />
				<br/>

				<div class="s-author">
					<br/>
					<p>Posted By <a href="#"><i class="fa fa-user" aria-hidden="true"></i> Admin</a> &nbsp;&nbsp; <i class="fa fa-calendar" aria-hidden="true"></i> June 2, 2016 &nbsp;&nbsp; <a href="#"><i class="fa fa-comments" aria-hidden="true"></i> Comments (10)</a></p>
				</div>

				<div class="agileinfo-news-top-grids"  >
					<div class="col-md-8 wthree-top-news-left" style="width: inherit;">
						<div class="wthree-news-left" style="width: inherit;">
							<div class="wthree-news-left-img">
								<div class="w3-agile-news-text">
									<p>Merkur se vraća svojoj uobičajenoj putanji trećeg maja. Sada vas više ništa ne može sputavati. Pun Mesec 10. maja može doneti rešenje za stari problem. Vaš šarm će biti moćniji od intelekta i logike. Povežite se na ličnom nivou, razbijte barijeru i steknite novog prijatelja. Za vreme Mladog Meseca 25. maja, prijatelji i komšije će možda pokušati da se pobrinu o vama. Doneće nove stvari. Možda će pokušati da posede i provedu vreme sa vama da bi vas naterali da malo usporite. Nemojte im se odupirati.
									</p>
								</div>
							</div>
						</div>
						

						
						<!-- agile-comments -->
						<div class="agile-news-comments">
							<div class="agile-news-comments-info">
								<h4>Ostavi komentar</h4>
									<div class="agile-info-wthree-box">
										<form>
											<textarea placeholder="Message" required=""></textarea>
											<input type="submit" value="SEND">
											<div class="clearfix"> </div>
										</form>
									</div>
							</div>
						</div>


						<div class="agile-news-comments">
							<div class="agile-news-comments-info">
								<h4>Komentari</h4>
									<div class="media">
										<h5>TOM BROWN</h5>
										<div class="media-left">
											<a href="#">
											<img src="<?php echo slike;?>user.jpg" title="One movies" alt=" " />
											</a>
										</div>
									<div class="media-body">
										<p>Maecenas ultricies rhoncus tincidunt maecenas imperdiet ipsum id ex pretium hendrerit maecenas imperdiet ipsum id ex pretium hendrerit</p>
										<span><i class="fa fa-calendar" aria-hidden="true"></i> June 2, 2016 &nbsp;&nbsp; </span>
								</div>
							</div>
								
							</div>
						</div>
						<!-- //agile-comments -->
						
					
					
						<!-- //news-right-top -->
						<!-- news-right-bottom -->
						
						<!-- //news-right-bottom -->
					</div>
					<div class="clearfix"> </div>

					
		</div>
	</div>
	</div>
<!-- //general -->