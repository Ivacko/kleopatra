<!-- Aleksandra Todorovic 0333/2014 -->
<div class="faq">
	<div class="container">
			
			<div><h4 class="latest-text w3_latest_text" style="margin-left: 0px">Početna</h4></div>

			<img src="<?php echo slike;?>about1.jpg" class="img-responsive" alt="" />
			<br/>

	</div>
</div>

<div class="container">
		<div class="agile-news-comments-info" >
		    <b>Karl Jung </b> – Rođeni smo u datom trenutku, na datom mestu, kao godišnja berba za vino, imamo kvalitetne sezone u našim životima. Astrologija ne polaže pravo na ništa više.<br/><br/>

			<b>Hipokrat</b> – Lekar bez znanja astrologije ne može sebe nazvati lekarom.<br/><br/>

			
			<b>Klark Ešton</b> – Čak i loš astrolog, povremeno, može pročitati nebesa ispravno.<br/><br/>
			
			<b>Šekspir</b> – Nije zvezdama namenjeno da drže sudbinu, ali u nama je to predodređeno.<br/><br/>

			<b>Šekspir</b> – Astrologija nema korisniju funkciju nego da otkrije najdublju prirodu čoveka i da je dovede u njegovu svest da bi on mogao da je ispuni sa zakonom svetlosti.<br/><br/>

			<b>Suzan Miler (astrolog)</b> – Astrologija nije proricanje sudbine. Kao agenti slobodne volje uvek imamo izbor. Mi obično imamo više izbora nego što mislimo. Naši horoskopi mogu predlagati mogućnosti koje smo prevideli.<br/><br/>

			<b>Albert Ajnštajn</b> - Astrologija sadrži znanje koje prosvetljuje. Naučila me je mnogo toga i puno joj dugujem. Geofizički dokazi otkrivaju moć zvezda i planeta u odnosu na Zemlju. Zauzvrat astrologija to potvrđuje. Zato je astrologija životni eliksir za čovečanstvo.<br/><br/>

			

			<b>Bendžamin Frenklin</b> - Astrologija je najstarija nauka, poštovana u prošlosti od Velikih i Mudrih. Nijedan kralj nije stvorio rat ili mir, niti jedan general borbu. Ukratko nijedna važna odluka nije doneta bez konsultacije sa astrologom.<br/><br/>

			<b>Klaudije Ptolemej</b> - Jasno je kako sve što se događa u prirodi ima svoj uzrok u događanjima na nebu.<br/><br/>

			<b>Pitagora</b> - Zvezde na nebu sviraju muziku. Kad bismo samo imali sluha da je čujemo.<br/><br/>
		      	
		</div>
	</div>
</div>	



























