<!-- Tanja Zivkovic 14/0469 -->
<div class="container">
	<div class="col-md-8 wthree-top-news-left" style="width: inherit;" >	
				<!-- agile-comments -->
		<div class="agile-news-comments-info" >	
			<table class="table">
			  <thead style="background-color: #FF8D1B;">
			    <tr >
			      <th style=" color:white;  ">#</th>
			      <th style=" color:white; ">Clanak</th>
			      <th style=" color: white; ">Datum</th>
			      <th style=" color: white; ">Tip</th>
			      <th style=" color: white;">Akcije</th>
			    </tr>
			  </thead>
			  <tbody >
			  <?php $i=1;
			  foreach ($clanci as $clanak) :?>
			  	<?php
			  	switch($clanak['Znak']){
			case 'OV': $data2['ZnakS']='Ovan';
				// $data2['Dat']='21. mart - 20. april';
				break;
			case 'BI': $data2['ZnakS']='Bik';
				// $data2['Dat']='21. april - 20. maj';
				break;
			case 'BL':{ $data2['ZnakS']='Blizanci';
				// $data2['Dat']='21. maj - 20. jun';
				break;}
			case 'RA': $data2['ZnakS']='Rak';
				// $data2['Dat']='21. jun - 20. jul';
				break;
			case 'LA': $data2['ZnakS']='Lav';
				// $data2['Dat']='21. jul - 21. avgust';
				break;
			case 'DE': $data2['ZnakS']='Devica';
				// $data2['Dat']='22. avgust - 22. septembar';
				break;
			case 'VA': $data2['ZnakS']='Vaga';
				// $data2['Dat']='23. septembar - 22. oktobar';
				break;
			case 'SK': $data2['ZnakS']='Skorpija';
				// $data2['Dat']='23. oktobar - 22. novembar';
				break;
			case 'ST': $data2['ZnakS']='Strelac';
				// $data2['Dat']='23. novembar - 21. decembar';
				break;
			case 'JA': $data2['ZnakS']='Jarac';
				// $data2['Dat']='22. decembar - 20. januar';
				break;
			case 'VO': $data2['ZnakS']='Vodolija';
				// $data2['Dat']='21. januar - 19. februar';
				break;
			case 'RI': $data2['ZnakS']='Ribe';
				// $data2['Dat']='20. februar - 20. mart';
				break;
			}
			switch($clanak['Tip']){
				case 'M':$tip='mesecni';break;
				case 'S':$tip='slaganje';break;
				case 'O':$tip='osobine';break;
			}
			  	?>
			    <tr>
			    	
				      <th scope="row"><?php echo $i;?></th>
				      <td> <?php echo $data2['ZnakS'];?> </td>
				      <td><?php echo $clanak['DatumOd'];?></td>
				      <td><?php echo $tip;?></td>
				      <td>
				      	<div class="col-md-8">
				      		<form action="<?php echo base_url()?>Clanak/otvori" method="post"> 
				      		 <input type="text" name='Id1' value="<?php echo $clanak['Id'];?>" style="display: none;">
				      		 <input type="text" name='Tip1' value="<?php echo $clanak['Tip'];?>" style="display: none;">
			              	<input type="submit" class="login loginmodal-submit" style="padding:5px 10px;" value="Pogledaj">
			              			<span></span>
			              	</form>
			              	<form action="<?php echo base_url()?>Clanak/obrisi" method="post"> 
				      		 <input type="text" name='Id2' value="<?php echo $clanak['Id'];?>" style="display: none;">
				      		 <input type="text" name='Tip2' value="<?php echo $clanak['Tip'];?>" style="display: none;">
			              	<input type="submit" class="login loginmodal-submit" style="padding:5px 10px;" value="Obrisi">
			              			<span></span>
			              	</form>
			            </div>
			           </td>
			           
			    </tr>
			    <?php
			    $i++; 
			    endforeach;?>
			  </tbody>
			</table>
		</div>
	</div>
</div>