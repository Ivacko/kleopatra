<!-- Aleksandra Todorovic 0333/2014 -->
<!-- nav -->
	<div class="movies_nav" >
		<div class="container">
			<nav class="navbar navbar-default">
				<div class="navbar-header navbar-left">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
					<nav>
						<ul class="nav navbar-nav">
							<li ><a href="<?php echo base_url()?>">Početna<br/>&nbsp;</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Mesečni<br/>horoskop<b class="caret"></b></a>
								<ul class="dropdown-menu multi-column columns">
									<li>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<li><a href="<?php echo base_url()?>clanak/otvori_clanak/OV">Ovan</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_clanak/BI">Bik</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_clanak/BL">Blizanac</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_clanak/RA">Rak</a></li>
										</ul>
									</div>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<li><a href="<?php echo base_url()?>clanak/otvori_clanak/LA">Lav</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_clanak/DE">Devica</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_clanak/VA">Vaga</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_clanak/SK">Škoprija</a></li>
										</ul>
									</div>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<li><a href="<?php echo base_url()?>clanak/otvori_clanak/ST">Strelac</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_clanak/JA">Jarac</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_clanak/VO">Vodolija</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_clanak/RI">Ribe</a></li>
										</ul>
									</div>
									<div class="clearfix"></div>
									</li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Osobine<br/> znakova<b class="caret"></b></a>
								<ul class="dropdown-menu multi-column columns">
									<li>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<li><a href="<?php echo base_url()?>clanak/otvori_osobine/OV">Ovan</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_osobine/BI">Bik</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_osobine/BL">Blizanac</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_osobine/RA">Rak</a></li>
										</ul>
									</div>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<li><a href="<?php echo base_url()?>clanak/otvori_osobine/LA">Lav</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_osobine/DE">Devica</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_osobine/VA">Vaga</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_osobine/SK">Škoprija</a></li>
										</ul>
									</div>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<li><a href="<?php echo base_url()?>clanak/otvori_osobine/ST">Strelac</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_osobine/JA">Jarac</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_osobine/VO">Vodolija</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_osobine/RI">Ribe</a></li>
										</ul>
									</div>
									<div class="clearfix"></div>
									</li>
								</ul>
							</li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">Slaganje <br/> znakova<b class="caret"></b></a>
								<ul class="dropdown-menu multi-column columns">
									<li>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<li><a href="<?php echo base_url()?>clanak/otvori_slaganje/OV">Ovan</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_slaganje/BI">Bik</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_slaganje/BL">Blizanac</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_slaganje/RA">Rak</a></li>
										</ul>
									</div>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<li><a href="<?php echo base_url()?>clanak/otvori_slaganje/LA">Lav</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_slaganje/DE">Devica</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_slaganje/VA">Vaga</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_slaganje/SK">Škoprija</a></li>
										</ul>
									</div>
									<div class="col-sm-4">
										<ul class="multi-column-dropdown">
											<li><a href="<?php echo base_url()?>clanak/otvori_slaganje/ST">Strelac</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_slaganje/JA">Jarac</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_slaganje/VO">Vodolija</a></li>
											<li><a href="<?php echo base_url()?>clanak/otvori_slaganje/RI">Ribe</a></li>
										</ul>
									</div>
									<div class="clearfix"></div>
									</li>
								</ul>
								
							</li>
							<li><a href="<?php echo base_url()?>gost/odredi_znak">Odredi<br/>svoj znak</a></li>
							<li><a href="<?php echo base_url()?>gost/about">O nama <br/> &nbsp;</a></li>
							<li><a href="<?php echo base_url()?>gost/contact">Kontakt <br/> &nbsp;</a></li>
							
						</ul>
					</nav>
				</div>
			</nav>	
		</div>
	</div>

	
<!-- //nav -->