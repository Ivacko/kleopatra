<!-- Aleksandra Todorovic 0333/2014 -->
<!DOCTYPE html>
<html lang="en">
<head>
<title>Kleopatra Veličanstvena</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="One Movies Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //for-mobile-apps -->


<link href="<?php echo css_stilovi;?>bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo css_stilovi;?>style.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo css_stilovi;?>contactstyle.css" rel="stylesheet"  type="text/css" media="all" />
<link href="<?php echo css_stilovi;?>faqstyle.css"  rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo css_stilovi;?>single.css" rel='stylesheet' type='text/css' />
<link href="<?php echo css_stilovi;?>medile.css" rel='stylesheet' type='text/css' />
<link href="<?php echo css_stilovi;?>jquery.slidey.min.css" rel="stylesheet">
<link href="<?php echo css_stilovi;?>owl.carousel.css" rel="stylesheet" type="text/css" media="all">
<link href="<?php echo css_stilovi;?>popuo-box.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo css_stilovi;?>font-awesome.min.css" rel="stylesheet"  />
<link href="<?php echo css_stilovi;?>news.css" rel="stylesheet"  type="text/css" media="all" />
<link href="<?php echo css_stilovi;?>news1.css" rel="stylesheet"  type="text/css" media="all" />
<link href="<?php echo css_stilovi;?>login.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo css_stilovi;?>list.css" rel="stylesheet"  type="text/css" media="all" />
<link href="<?php echo css_stilovi;?>font-awesome.min.css" rel="stylesheet" />


<script type="text/javascript" src="<?php echo javascript_fje;?>jquery-2.1.4.min.js"></script>
<script src="<?php echo javascript_fje;?>owl.carousel.js"></script>
<script type="text/javascript" src="<?php echo javascript_fje;?>move-top.js"></script>
<script type="text/javascript" src="<?php echo javascript_fje;?>easing.js"></script>
<script type="text/javascript" src="<?php echo javascript_fje;?>jquery.tools.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo javascript_fje?>bootstrap.min.js"></script>
<script>
$(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
            $(this).toggleClass('open');       
        }
    );
});
</script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
	
	function Prijatelj()
	{
		window.open("<?php echo base_url()?>korisnik/prijatelj",'_self',false)

	}

	function Stranac()
	{
		window.open("<?php echo base_url()?>korisnik/stranac",'_self',false)

	}
</script>
<!-- start-smoth-scrolling -->
<style>

</style>
</head>
	
<body>

