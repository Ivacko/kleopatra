
<?php

	class Pretraga extends CI_Controller{



			public function zahtev(){

			session_start();
			$this->load->model('pretraga_korisnika');
		
			
				$data = array(
					'ime' => $_SESSION['ime'],
					'prezime' => $_SESSION['prezime'],
					'id' => $_SESSION['id']

				);



			$zahtevi['niz'] = $this->pretraga_korisnika->getKorisnika($data['id']);


			//print_r($zahtevi['niz']['0']['Ime']);
			$n = count($zahtevi['niz']);
			$data2 = null;

			for($i = 0; $i < $n; $i++){

				$mesto = $this->pretraga_korisnika->getMesto($zahtevi['niz'][$i]['IdOd']);

				$red = array(
					'ime' => $zahtevi['niz'][$i]['Ime'],
					'prezime' => $zahtevi['niz'][$i]['Prezime'],
					'mesto' => $mesto['Mesto'],
					'datum' => $mesto['DatumRodjenja'],
					'id' => $zahtevi['niz'][$i]['IdOd']
		);
				$data2['redovi']['red'.$i] = $red;

			}

     		$this->load->view('templates/header');
			$this->load->view('korisnik/header', $data);
			
			$this->load->view('templates/meni');
			$this->load->view('korisnik/meni');
			$this->load->view('pretrage/obavestenja', $data2);
			$this->load->view('templates/footer');



			}


			public function index(){
				session_start();
				
				if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'K'){
				$data = array(
					'ime' => $_SESSION['ime'],
					'prezime' => $_SESSION['prezime']
					);
			}
			else{
				redirect('gosti');
			}

			$this->load->model('pretraga_korisnika');
			$mojId = $_SESSION['id'];
			$fleg = 5;

			$row = $this->pretraga_korisnika->getRegularan($mojId);
			$mojZnak = $row->Znak;

			//sad mi treba niz znakova koji se slazu sa mnom
			$slaganje['niz'] = $this->pretraga_korisnika->getSlaganje($mojZnak);

			//zatim uzimam osobine tih znakova iz clanka i postavljam ih
			for($m = 0; $m < 3; $m++){

				$znak = $this->pretraga_korisnika->getZnak($slaganje['niz'][''.$m]['ZnakZa']);
				if( $znak != NULL) {
				$ime = $this->pretraga_korisnika->zameniZnak($znak['Znak']);
				//print_r($ime);
				//print_r($znak['Tekst']);
				$znakovi = array(
					'znak' =>  $ime,
					'text' => $znak['Tekst']
					);
				$podaciIzClankaOZnaku['redovi'][''.$m] = $znakovi;
				$fleg = 3;
				}else{
					$znakovi = array(
					'znak' =>  "",
					'text' => ""
					);
				$podaciIzClankaOZnaku['redovi'][''.$m] = $znakovi;
				}

			}


			$n = 3;
			$q = 1;
			$data2 = null;

			for($i = 0; $i < $n; $i++){
				if( $fleg != 5){
				$novired = array(
					'imeZnak' => $podaciIzClankaOZnaku['redovi'][''.$i]['znak'],
					'text' => $podaciIzClankaOZnaku['redovi'][''.$i]['text'],
		'procenat' => number_format(($slaganje['niz'][''.$i]['OcenaPoz']/($slaganje['niz'][''.$i]['OcenaPoz'] + $slaganje['niz'][''.$i]['OcenaNeg']))*100, 2),
					'znak' => $podaciIzClankaOZnaku['redovi'][''.$i]['znak']

					);
				$data2['redovi']['red'.$i] = $novired;
				$q++;
				}else{
					$nula = 0;
					$novired = array(
					'imeZnak' => $podaciIzClankaOZnaku['redovi'][''.$i]['znak'],
					'text' => $podaciIzClankaOZnaku['redovi'][''.$i]['text'],
		'procenat' => $nula ,
					'znak' => $podaciIzClankaOZnaku['redovi'][''.$i]['znak']

					);
				$data2['redovi']['red'.$i] = $novired;
					
				}

			}

			$this->load->view('templates/header');
			$this->load->view('korisnik/header', $data);
			
			$this->load->view('templates/meni');
			$this->load->view('korisnik/meni');
			$this->load->view('pretrage/search', $data2);
			$this->load->view('templates/footer');

				
			}


			


			public function ucitajStranicu($znak = 'Bi'){

				session_start();
				$this->load->model('pretraga_korisnika');

			
				
				if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'K'){
				$data = array(
					'ime' => $_SESSION['ime'],
					'prezime' => $_SESSION['prezime'],
					'pol' => $_SESSION['pol'],
					'znak' => $znak
					);
			}
			else{
				redirect('gosti');
			}
			if($_SESSION['pol'] == 'Z')
				$data['pol'] = 'M';
			else
				$data['pol'] = 'Z';


			


			//vraca mi korisnike koji mi odgovaraju po znaku i polu
			//sad od tih korisnika nadji samo one koji zive u Loznici
			$nizKorisnika['niz'] = $this->pretraga_korisnika->getKorisnike($data);

			$data2 = array(
					'mesto' => $this->input->post('mesto'),
					'number' => $this->input->post('number'),
					'od' => $this->input->post('od'),
					'do' => $this->input->post('do')
					);

			
			//$korisnici['niz'] = $this->pretraga_korisnika->dohvatiKorisnike($data2);

			$this->load->helper('date');
			


			$datestring = '%Y';
			$time = time();
			$danas = mdate($datestring, $time);
			// echo mdate($datestring, $time);
			
			

			$n = $data2['number'];
			$j = 0;
			//print_r($nizKorisnika);

			



			for($i = 1; $i < $n; $i++){

				if(isset($data2['mesto']) && $nizKorisnika['niz'][''.$i]['Mesto'] == $data2['mesto']){

					$newDate = date("Y", strtotime($nizKorisnika['niz'][''.$i]['DatumRodjenja']));
					
					$odd = $data2['od'];
					$doo = $data2['do'];
					$razlika = $danas - $newDate;

					if( $odd <= $razlika ){
						if( $razlika <= $doo){
						$novired = array(
						'id' => $nizKorisnika['niz'][''.$i]['Id'],
						'ime' => $nizKorisnika['niz'][''.$i]['Ime'],
						'rodjendan' => $nizKorisnika['niz'][''.$i]['DatumRodjenja'],
						'mesto' => $nizKorisnika['niz'][''.$i]['Mesto']

						);

						$data3['redovi']['red'.$j] = $novired;
						$j++;
					}
				}

				

				}

			}
			


			


			$this->load->view('templates/header');
			$this->load->view('korisnik/header', $data);
			
			$this->load->view('templates/meni');
			$this->load->view('korisnik/meni');

			if($j > 0){

				$this->load->view('pretrage/Znakovi1', $data3);
			}else{

				$this->load->view('pretrage/Znakovi', $nizKorisnika);
			}

			
			$this->load->view('templates/footer');

			}		



			public function ucitajProfil($pod){

				session_start();
				
				$this->load->model('pretraga_korisnika');

				$data = array(
					'ime' => $_SESSION['ime'],
					'prezime' => $_SESSION['prezime'],
					'id' => $_SESSION['id']

				);

				$korisnik['niz'] = $this->pretraga_korisnika->getProfil($pod);


			$this->load->view('templates/header');
			$this->load->view('korisnik/header', $data);
			
			$this->load->view('templates/meni');
			$this->load->view('korisnik/meni');
			$this->load->view('korisnik/stranac', $korisnik);
			$this->load->view('templates/footer');


			}

			public function ucitajProfil2($pod){

				session_start();
				$this->load->model('pretraga_korisnika');

				$data = array(
					'ime' => $_SESSION['ime'],
					'prezime' => $_SESSION['prezime'],
					'id' => $_SESSION['id']

				);

				$korisnik['niz'] = $this->pretraga_korisnika->getProfil($pod);


			$this->load->view('templates/header');
			$this->load->view('korisnik/header', $data);
			
			$this->load->view('templates/meni');
			$this->load->view('korisnik/meni');
			$this->load->view('korisnik/noStranac', $korisnik);
			$this->load->view('templates/footer');


			}



			public function dodaj($id){
					session_start();
					$this->load->model('pretraga_korisnika');

				$mojId = $_SESSION['id'];
				$mojPol =  $_SESSION['pol'];

				if($mojId != $mojPol){
					if($mojId == 'Z'){
						$data = array(
						'idM' => $id,
						'idZ' => $mojId
						// 'OcenaM' => null,
						// 'OcenaZ' => null,
						// 'datumOd' => null
							
							);
					}else{
						$data = array(
						'idM' => $mojId,
						'idZ' => $id
						// 'OcenaM' => null,
						// 'OcenaZ' => null,
						// 'datumOd' => null
							
							);

					}
				}

				$this->pretraga_korisnika->dodajKontakt($data);

			}


	
	}