<?php
	
	/**
	* Aleksandra Todorovic 0333/2104
	*/
	class Moderatori extends CI_Controller	{

		public function index(){
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'M'){
				$data = array(
					'username' => $_SESSION['username'],
					'komentari'=> $this->komentari_model->getNeobjavljeni('X')
					);
			}
			else{
				redirect('gosti');
			}

			$this->load->view('templates/header');
			$this->load->view('moderator/header', $data);
			
			$this->load->view('templates/meni');
			$this->load->view('moderator/meni');
			$this->load->view('templates/index');
		

			$this->load->view('templates/footer');

		}
		
		public function view($page='index')
		{
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'M'){
				$data = array(
					'username' => $_SESSION['username'],
					'komentari'=> $this->komentari_model->getNeobjavljeni('X')
					);
			}
			else{
				redirect('gosti');
			}
			

			if (file_exists(APPPATH.'views/moderator/'. $page. '.php'))
			{
				$this->load->view('templates/header');
				$this->load->view('moderator/header', $data);

				$this->load->view('templates/meni');
				$this->load->view('moderator/meni');
				$this->load->view('moderator/'.$page);
			

				$this->load->view('templates/footer');
			}
			elseif (file_exists(APPPATH.'views/templates/'. $page. '.php'))
			{
					$this->load->view('templates/header');
					$this->load->view('moderator/header', $data);

					$this->load->view('templates/meni');
					$this->load->view('moderator/meni');
					$this->load->view('templates/'.$page);
				

					$this->load->view('templates/footer');
			}
			else
			{
				show_404();
			}
			
		}

		public function izbrisiKomentar()
		{
			$this->komentari_model->izbrisiKomentar();
			redirect('moderator/komentari');
		}
		public function objaviKomentar()
		{
			$q=$this->komentari_model->objaviKomentar();
			redirect('moderator/komentari');
			
		}
		/*Tanja Zivkovic 14/0469*/
		public function napisi_clanak(){
			session_start();
			$data['username']=$_SESSION['username'];
			$this->load->library('form_validation');
			$this->form_validation->set_rules('tip_cl', 'Tip clanka', 'required');
			$this->form_validation->set_rules('znak', 'znak', 'required');
			if($this->form_validation->run() == FALSE){
				$this->napisi_clanak();
			}else{
			$data['Tip']=$this->input->post('tip_cl');
			$data['Znak']=$this->input->post('znak');
			$_SESSION['Tip']=$data['Tip'];
			$_SESSION['Znak']=$data['Znak'];
			switch($data['Znak']){
			case 'OV': $data['ZnakS']='ovan';
				$data['Dat']='21. mart - 20. april';
				break;
			case 'BI': $data['ZnakS']='bik';
				$data['Dat']='21. april - 20. maj';
				break;
			case 'BL':{ $data['ZnakS']='blizanci';
				$data['Dat']='21. maj - 20. jun';
				break;}
			case 'RA': $data['ZnakS']='rak';
				$data['Dat']='21. jun - 20. jul';
				break;
			case 'LA': $data['ZnakS']='lav';
				$data['Dat']='21. jul - 21. avgust';
				break;
			case 'DE': $data['ZnakS']='devica';
				$data['Dat']='22. avgust - 22. septembar';
				break;
			case 'VA': $data['ZnakS']='vaga';
				$data['Dat']='23. septembar - 22. oktobar';
				break;
			case 'SK': $data['ZnakS']='skorpija';
				$data['Dat']='23. oktobar - 22. novembar';
				break;
			case 'ST': $data['ZnakS']='strelac';
				$data['Dat']='23. novembar - 21. decembar';
				break;
			case 'JA': $data['ZnakS']='jarac';
				$data['Dat']='22. decembar - 20. januar';
				break;
			case 'VO': $data['ZnakS']='vodolija';
				$data['Dat']='21. januar - 19. februar';
				break;
			case 'RI': $data['ZnakS']='ribe';
				$data['Dat']='20. februar - 20. mart';
				break;
			}
			$_SESSION['ZnakS']=$data['ZnakS'];
			switch($data['Tip']){
				case 'M':
					$data['Ljubav']="";
					$data['Posao']="";
					$data['Zdravlje']="";
					$this->load->view('templates/header');
					$this->load->view('moderator/header', $data);
					$this->load->view('templates/meni');
					If($_SESSION['tip']=='M')
					$this->load->view('moderator/meni');
					else $this->load->view('admin/meni');
					$this->load->view('templates/napisi_mesecni', $data);
					$this->load->view('templates/footer');
						break;
				case 'O':
					$data['Tekst']="";
					$this->load->view('templates/header');
					$this->load->view('moderator/header', $data);
					$this->load->view('templates/meni');
					If($_SESSION['tip']=='M')
					$this->load->view('moderator/meni');
					else $this->load->view('admin/meni');
					$this->load->view('templates/napisi_osobine', $data);
					$this->load->view('templates/footer');
					break;
				case 'S':

					$this->load->view('templates/header');
					$this->load->view('moderator/header', $data);
					$this->load->view('templates/meni');
					If($_SESSION['tip']=='M')
					$this->load->view('moderator/meni');
					else $this->load->view('admin/meni');
					$this->load->view('templates/napisi_slaganje', $data);
					$this->load->view('templates/footer');
				}
			
			}
		}

		public function napisi_mesecni(){
				$this->load->helper('date');
				session_start();
				$datac['Tip']=$_SESSION['Tip'];
				$datac['Znak']=$_SESSION['Znak'];	
				$datac['DatumOd']=$this->input->post('DatumOd');

				$this->load->model('Clanak_model');
					$datac['IdRezervator']=$_SESSION['id'];
					$datak=$this->Clanak_model->ubaci_clanak($datac);
					//print_r("ubaceno u clanak");
					$datam['Id']=$datak;
					$datam['Tip']='M';
					$_SESSION['IdClan']=$datak;
					$datam['Znak']=$_SESSION['Znak'];
					$dataa['Datum']=date('y-m-d');
					$datam['Ljubav']=$this->input->post('Ljubav');
					$datam['Posao']=$this->input->post('Posao');
					$datam['Zdravlje']=$this->input->post('Zdravlje');
					$this->Clanak_model->ubaci_mesecni($datam);
					$dataa['IdMod']=$_SESSION['id'];
					$dataa['IdClan']=$datam['Id'];
					$dataa['Datum']=date('Y-m-d');
					$this->Clanak_model->ubaci_autora($dataa);
					$datac['Ljubav']=$datam['Ljubav'];
					$datac['Posao']=$datam['Posao'];
					$datac['Zdravlje']=$datam['Zdravlje'];
					$datac['username']=$_SESSION['username'];
					$datac['Tip']=$_SESSION['Tip'];
					$datac['Znak']=$_SESSION['Znak'];
					switch($datac['Znak']){
					case 'OV': $datac['ZnakS']='ovan';
						$datac['Dat']='21. mart - 20. april';
						break;
					case 'BI': $datac['ZnakS']='bik';
						$datac['Dat']='21. april - 20. maj';
						break;
					case 'BL':{ $datac['ZnakS']='blizanci';
						$datac['Dat']='21. maj - 20. jun';
						break;}
					case 'RA': $datac['ZnakS']='rak';
						$datac['Dat']='21. jun - 20. jul';
						break;
					case 'LA': $datac['ZnakS']='lav';
						$datac['Dat']='21. jul - 21. avgust';
						break;
					case 'DE': $datac['ZnakS']='devica';
						$datac['Dat']='22. avgust - 22. septembar';
						break;
					case 'VA': $datac['ZnakS']='vaga';
						$datac['Dat']='23. septembar - 22. oktobar';
						break;
					case 'SK': $datac['ZnakS']='skorpija';
						$datac['Dat']='23. oktobar - 22. novembar';
						break;
					case 'ST': $datac['ZnakS']='strelac';
						$datac['Dat']='23. novembar - 21. decembar';
						break;
					case 'JA': $datac['ZnakS']='jarac';
						$datac['Dat']='22. decembar - 20. januar';
						break;
					case 'VO': $datac['ZnakS']='vodolija';
						$datac['Dat']='21. januar - 19. februar';
						break;
					case 'RI': $datac['ZnakS']='ribe';
						$datac['Dat']='20. februar - 20. mart';
						break;
					}	
					$datac['kom']=null;
					$datac['tip']=null;
					//print_r($datac);
					$this->load->view('templates/header');
					$this->load->view('moderator/header', $datac);
					$this->load->view('templates/meni');
					If($_SESSION['tip']=='M')
					$this->load->view('moderator/meni');
					else $this->load->view('admin/meni');
					$this->load->view('clanak/edit_mesecni',$datac);
					$this->load->view('templates/footer');
		}

		public function prosledi_mesecni(){
				session_start();
				$data['Tip']=$_SESSION['tip'];
				$data['Znak']=$_SESSION['Znak'];
				$data['Ljubav']=$this->input->post('Ljubav');
				$data['Posao']=$this->input->post('Posao');
				$data['Zdravlje']=$this->input->post('Zdravlje');
				$data['DatumOd']=$this->input->post('DatumOd');
				$data['Id']=$_SESSION['IdClan'];
				$data['username']=$_SESSION['username'];
				switch($data['Znak']){
					case 'OV': $data['ZnakS']='ovan';
						$data['Dat']='21. mart - 20. april';
						break;
					case 'BI': $data['ZnakS']='bik';
						$data['Dat']='21. april - 20. maj';
						break;
					case 'BL':{ $data['ZnakS']='blizanci';
						$data['Dat']='21. maj - 20. jun';
						break;}
					case 'RA': $data['ZnakS']='rak';
						$data['Dat']='21. jun - 20. jul';
						break;
					case 'LA': $data['ZnakS']='lav';
						$data['Dat']='21. jul - 21. avgust';
						break;
					case 'DE': $data['ZnakS']='devica';
						$data['Dat']='22. avgust - 22. septembar';
						break;
					case 'VA': $data['ZnakS']='vaga';
						$data['Dat']='23. septembar - 22. oktobar';
						break;
					case 'SK': $data['ZnakS']='skorpija';
						$data['Dat']='23. oktobar - 22. novembar';
						break;
					case 'ST': $data['ZnakS']='strelac';
						$data['Dat']='23. novembar - 21. decembar';
						break;
					case 'JA': $data['ZnakS']='jarac';
						$data['Dat']='22. decembar - 20. januar';
						break;
					case 'VO': $data['ZnakS']='vodolija';
						$data['Dat']='21. januar - 19. februar';
						break;
					case 'RI': $data['ZnakS']='ribe';
						$data['Dat']='20. februar - 20. mart';
						break;
					}
				//print_r($data);
				$this->load->view('templates/header');
				$this->load->view('moderator/header', $data);
				$this->load->view('templates/meni');
				$this->load->view('moderator/meni');
				$this->load->view('clanak/forma_edit_mesecni',$data);
				$this->load->view('templates/footer');
			}


		public function izmeni_mesecni(){
				session_start();
				$this->load->helper('date');
				$datac['Tip']=$_SESSION['Tip'];
				$datac['Znak']=$_SESSION['Znak'];
				$this->load->model('Clanak_model');
					$datac['Id']=$_SESSION['IdClan'];
					$datac['Ljubav']=$this->input->post('Ljubav');
					$datac['Posao']=$this->input->post('Posao');
					$datac['Zdravlje']=$this->input->post('Zdravlje');
					//print_r($datac);
					$this->Clanak_model->izmeni_mesecni($datac);
					$datac['DatumOd']=$this->input->post('DatumOd');
					$datac['username']=$_SESSION['username'];
					switch($datac['Znak']){
					case 'OV': $datac['ZnakS']='ovan';
						$datac['Dat']='21. mart - 20. april';
						break;
					case 'BI': $datac['ZnakS']='bik';
						$datac['Dat']='21. april - 20. maj';
						break;
					case 'BL':{ $datac['ZnakS']='blizanci';
						$datac['Dat']='21. maj - 20. jun';
						break;}
					case 'RA': $datac['ZnakS']='rak';
						$datac['Dat']='21. jun - 20. jul';
						break;
					case 'LA': $datac['ZnakS']='lav';
						$datac['Dat']='21. jul - 21. avgust';
						break;
					case 'DE': $datac['ZnakS']='devica';
						$datac['Dat']='22. avgust - 22. septembar';
						break;
					case 'VA': $datac['ZnakS']='vaga';
						$datac['Dat']='23. septembar - 22. oktobar';
						break;
					case 'SK': $datac['ZnakS']='skorpija';
						$datac['Dat']='23. oktobar - 22. novembar';
						break;
					case 'ST': $datac['ZnakS']='strelac';
						$datac['Dat']='23. novembar - 21. decembar';
						break;
					case 'JA': $datac['ZnakS']='jarac';
						$datac['Dat']='22. decembar - 20. januar';
						break;
					case 'VO': $datac['ZnakS']='vodolija';
						$datac['Dat']='21. januar - 19. februar';
						break;
					case 'RI': $datac['ZnakS']='ribe';
						$datac['Dat']='20. februar - 20. mart';
						break;
					}	
					$datac['kom']=null;
					//$datac['tip']=null;
					//print_r($datac);
					$this->load->view('templates/header');
					$this->load->view('moderator/header', $datac);
					$this->load->view('templates/meni');
					$this->load->view('moderator/meni');
					$this->load->view('clanak/edit_mesecni',$datac);
					$this->load->view('templates/footer');
		}
		

		public function napisi_osobine(){
				session_start();
				$data['Tip']=$_SESSION['Tip'];
				$data['Znak']=$_SESSION['Znak'];
				$data['DatumOd']=$this->input->post('DatumOd');
				$_SESSION['DatumOd']=$data['DatumOd'];
				$this->load->model('Clanak_model');
				$data['Tekst']=$this->input->post('Tekst');
				$data['IdRezervator']=$_SESSION['id'];
				$datam=$this->Clanak_model->ubaci_clanak($data);
				$_SESSION['IdClan']=$datam;
				$dataa['IdMod']=$_SESSION['id'];
				$dataa['IdClan']=$datam;
				$dataa['Datum']=date('y-m-d');
				$this->Clanak_model->ubaci_autora($dataa);
				$data['username']=$_SESSION['username'];
				$data['Id']=$datam;
				switch($data['Znak']){
					case 'OV': $data['ZnakS']='ovan';
						$data['Dat']='21. mart - 20. april';
						break;
					case 'BI': $data['ZnakS']='bik';
						$data['Dat']='21. april - 20. maj';
						break;
					case 'BL':{ $data['ZnakS']='blizanci';
						$data['Dat']='21. maj - 20. jun';
						break;}
					case 'RA': $data['ZnakS']='rak';
						$data['Dat']='21. jun - 20. jul';
						break;
					case 'LA': $data['ZnakS']='lav';
						$data['Dat']='21. jul - 21. avgust';
						break;
					case 'DE': $data['ZnakS']='devica';
						$data['Dat']='22. avgust - 22. septembar';
						break;
					case 'VA': $data['ZnakS']='vaga';
						$data['Dat']='23. septembar - 22. oktobar';
						break;
					case 'SK': $data['ZnakS']='skorpija';
						$data['Dat']='23. oktobar - 22. novembar';
						break;
					case 'ST': $data['ZnakS']='strelac';
						$data['Dat']='23. novembar - 21. decembar';
						break;
					case 'JA': $data['ZnakS']='jarac';
						$data['Dat']='22. decembar - 20. januar';
						break;
					case 'VO': $data['ZnakS']='vodolija';
						$data['Dat']='21. januar - 19. februar';
						break;
					case 'RI': $data['ZnakS']='ribe';
						$data['Dat']='20. februar - 20. mart';
						break;
					}
					//print_r($data);
				$this->load->view('templates/header');
				$this->load->view('moderator/header', $data);
				$this->load->view('templates/meni');
				$this->load->view('moderator/meni');
				$this->load->view('clanak/edit_osobine',$data);
				$this->load->view('templates/footer');


		}

		public function prosledi_osobine(){
				session_start();
				$data['Tip']=$_SESSION['Tip'];
				$data['Znak']=$_SESSION['Znak'];
				$data['Tekst']=$this->input->post('Tekst');
				$data['DatumOd']=$_SESSION['DatumOd'];
				$data['Id']=$_SESSION['IdClan'];
				$data['username']=$_SESSION['username'];
				switch($data['Znak']){
					case 'OV': $data['ZnakS']='ovan';
						$data['Dat']='21. mart - 20. april';
						break;
					case 'BI': $data['ZnakS']='bik';
						$data['Dat']='21. april - 20. maj';
						break;
					case 'BL':{ $data['ZnakS']='blizanci';
						$data['Dat']='21. maj - 20. jun';
						break;}
					case 'RA': $data['ZnakS']='rak';
						$data['Dat']='21. jun - 20. jul';
						break;
					case 'LA': $data['ZnakS']='lav';
						$data['Dat']='21. jul - 21. avgust';
						break;
					case 'DE': $data['ZnakS']='devica';
						$data['Dat']='22. avgust - 22. septembar';
						break;
					case 'VA': $data['ZnakS']='vaga';
						$data['Dat']='23. septembar - 22. oktobar';
						break;
					case 'SK': $data['ZnakS']='skorpija';
						$data['Dat']='23. oktobar - 22. novembar';
						break;
					case 'ST': $data['ZnakS']='strelac';
						$data['Dat']='23. novembar - 21. decembar';
						break;
					case 'JA': $data['ZnakS']='jarac';
						$data['Dat']='22. decembar - 20. januar';
						break;
					case 'VO': $data['ZnakS']='vodolija';
						$data['Dat']='21. januar - 19. februar';
						break;
					case 'RI': $data['ZnakS']='ribe';
						$data['Dat']='20. februar - 20. mart';
						break;
					}
				//print_r($data);
				$this->load->view('templates/header');
				$this->load->view('moderator/header', $data);
				$this->load->view('templates/meni');
				$this->load->view('moderator/meni');
				$this->load->view('clanak/forma_edit_osobine',$data);
				$this->load->view('templates/footer');
		}

		public function izmeni_osobine(){
				session_start();
				$data['Tip']=$_SESSION['Tip'];
				$data['Znak']=$_SESSION['Znak'];
				$data['DatumOd']=$_SESSION['DatumOd'];;
				$this->load->model('Clanak_model');
				$data['Tekst']=$this->input->post('Tekst');
				$data['Id']=$_SESSION['IdClan'];
				$this->Clanak_model->izmeni_clanak($data);
				$data['username']=$_SESSION['username'];
				switch($data['Znak']){
					case 'OV': $data['ZnakS']='ovan';
						$data['Dat']='21. mart - 20. april';
						break;
					case 'BI': $data['ZnakS']='bik';
						$data['Dat']='21. april - 20. maj';
						break;
					case 'BL':{ $data['ZnakS']='blizanci';
						$data['Dat']='21. maj - 20. jun';
						break;}
					case 'RA': $data['ZnakS']='rak';
						$data['Dat']='21. jun - 20. jul';
						break;
					case 'LA': $data['ZnakS']='lav';
						$data['Dat']='21. jul - 21. avgust';
						break;
					case 'DE': $data['ZnakS']='devica';
						$data['Dat']='22. avgust - 22. septembar';
						break;
					case 'VA': $data['ZnakS']='vaga';
						$data['Dat']='23. septembar - 22. oktobar';
						break;
					case 'SK': $data['ZnakS']='skorpija';
						$data['Dat']='23. oktobar - 22. novembar';
						break;
					case 'ST': $data['ZnakS']='strelac';
						$data['Dat']='23. novembar - 21. decembar';
						break;
					case 'JA': $data['ZnakS']='jarac';
						$data['Dat']='22. decembar - 20. januar';
						break;
					case 'VO': $data['ZnakS']='vodolija';
						$data['Dat']='21. januar - 19. februar';
						break;
					case 'RI': $data['ZnakS']='ribe';
						$data['Dat']='20. februar - 20. mart';
						break;
					}
				$this->load->view('templates/header');
				$this->load->view('moderator/header', $data);
				$this->load->view('templates/meni');
				$this->load->view('moderator/meni');
				$this->load->view('clanak/edit_osobine',$data);
				$this->load->view('templates/footer');
		}

		public function napisi_slaganje(){
				session_start();
				$znakovi=array('Ovan', 'Bik', 'Blizanci', 'Rak', 'Lav', 'Devica', 'Vaga', 'Škorpija', 'Strelac', 'Jarac', 'Vodolija', 'Ribe');
				$znakovik=array('OV', 'BI', 'BL', 'RA', 'LA', 'DE', 'VA', 'SK', 'ST', 'JA','VO', 'RI');
				$data['Tip']=$_SESSION['Tip'];
				$data['Znak']=$_SESSION['Znak'];
				$data['DatumOd']=$this->input->post('DatumOd');
				$this->load->model('Clanak_model');
				$i=0;
				foreach ($znakovik as $jedan) {
					$data['Tekst']=$this->input->post("Tekst".$znakovi[$i]);
					$data['IdRezervator']=$_SESSION['id'];
					//print_r($data);
					$datam=$this->Clanak_model->ubaci_clanak($data);
					$datas['Id']=$datam;
					$datas['Tip']=$data['Tip'];
					$datas['ZnakOd']=$data['Znak'];
					$datas['ZnakZa']=$jedan;
					$datas['OcenaPoz']=$this->input->post("OcenaPoz".$znakovi[$i]);
					$datas['OcenaNeg']=$this->input->post("OcenaNeg".$znakovi[$i]);
					//print_r($datas);
					$this->Clanak_model->ubaci_slaganje($datas);
					$dataa['IdMod']=$_SESSION['id'];
					$dataa['IdClan']=$datam;
					$dataa['Datum']=date('y-m-d');
					$this->Clanak_model->ubaci_autora($dataa);
					$i++;
				}
				$data['niz']=$this->Clanak_model->dohvati_slaganje($data['Znak']);
				//print_r($data['niz']);
				$data['username']=$_SESSION['username'];
				switch($data['Znak']){
			case 'OV': $data['ZnakS']='ovan';
				$data['Dat']='21. mart - 20. april';
				break;
			case 'BI': $data['ZnakS']='bik';
				$data['Dat']='21. april - 20. maj';
				break;
			case 'BL': $data['ZnakS']='blizanci';
				$data['Dat']='21. maj - 20. jun';
				break;
			case 'RA': $data['ZnakS']='rak';
				$data['Dat']='21. jun - 20. jul';
				break;
			case 'LA': $data['ZnakS']='lav';
				$data['Dat']='21. jul - 21. avgust';
				break;
			case 'DE': $data['ZnakS']='devica';
				$data['Dat']='22. avgust - 22. septembar';
				break;
			case 'VA': $data['ZnakS']='vaga';
				$data['Dat']='23. septembar - 22. oktobar';
				break;
			case 'SK': $data['ZnakS']='skorpija';
				$data['Dat']='23. oktobar - 22. novembar';
				break;
			case 'ST': $data['ZnakS']='strelac';
				$data['Dat']='23. novembar - 21. decembar';
				break;
			case 'JA': $data['ZnakS']='jarac';
				$data['Dat']='22. decembar - 20. januar';
				break;
			case 'VO': $data['ZnakS']='vodolija';
				$data['Dat']='21. januar - 19. februar';
				break;
			case 'RI': $data['ZnakS']='ribe';
				$data['Dat']='20. februar - 20. mart';
				break;
			}
			$data['username']=$_SESSION['username'];
			//print_r($data);
			$this->load->view('templates/header');
			$this->load->view('moderator/header', $data);
			$this->load->view('templates/meni');
			$this->load->view('moderator/meni');
			$this->load->view('clanak/slaganje',$data);
			$this->load->view('templates/footer');

		}

		public function moji_clanci(){
			session_start();
			$this->load->model('Clanak_model');
			$data['Id']=$_SESSION['id'];
			$data['clanci']=$this->Clanak_model->dohvati_moje($data['Id']);
			$data['username']=$_SESSION['username'];
			$this->load->view('templates/header');
			$this->load->view('moderator/header', $data);
			$this->load->view('templates/meni');
			$this->load->view('moderator/meni');
			$this->load->view('templates/mojiclanci',$data);
			$this->load->view('templates/footer');
			}

	}