<?php /*Tanja Zivkovic 14/0469*/
	class Clanak extends CI_Controller{
		public function index(){
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'K'){
				$data = array(
					'ime' => $_SESSION['ime'],
					'prezime' => $_SESSION['prezime']
					);
			}
			$data['title']="Arhiva";

			$this->load->model('Clanak_model');
			$data['posts']=$this->Clanak_model->dohvati_clanke();
			//print_r($data['posts']);
			if(isset($data['posts']) == false) echo LALALA;

			$this->load->view('templates/header');
			$this->load->view('gost/header');

			$this->load->view('templates/meni');
			$this->load->view('clanak/index',$data);

			$this->load->view('templates/footer');

		}

		public function otvori_clanak($znak){
			session_start();
			$_SESSION['znakCl']=$znak;
			$this->load->model('Clanak_model');
			$data=$this->Clanak_model->dohvati_zadnji($znak);
			//print_r($data);
			//print_r($data['DatumOd']."plus 1 mesec".date('Y-m-d',strtotime("+1 month")+$data['DatumOd']));
			if(isset($_SESSION['tip']) and $_SESSION['tip']=='K'){
				$data['ime'] = $_SESSION['ime'];
				$data['prezime'] = $_SESSION['prezime'];
				$data['tip'] = $_SESSION['tip'];
			}
			else if(isset($_SESSION['tip'])){
				$data['username']=$_SESSION['username'];
				$data['tip'] = $_SESSION['tip'];
				//print_r("lalala");
			}
			if (!isset($_SESSION['tip'])) $data['tip']=null;

			switch($znak){
			case 'OV': $data['ZnakS']='ovan';
				$data['Dat']='21. mart - 20. april';
				break;
			case 'BI': $data['ZnakS']='bik';
				$data['Dat']='21. april - 20. maj';
				break;
			case 'BL':{ $data['ZnakS']='blizanci';
				$data['Dat']='21. maj - 20. jun';
				break;}
			case 'RA': $data['ZnakS']='rak';
				$data['Dat']='21. jun - 20. jul';
				break;
			case 'LA': $data['ZnakS']='lav';
				$data['Dat']='21. jul - 21. avgust';
				break;
			case 'DE': $data['ZnakS']='devica';
				$data['Dat']='22. avgust - 22. septembar';
				break;
			case 'VA': $data['ZnakS']='vaga';
				$data['Dat']='23. septembar - 22. oktobar';
				break;
			case 'SK': $data['ZnakS']='skorpija';
				$data['Dat']='23. oktobar - 22. novembar';
				break;
			case 'ST': $data['ZnakS']='strelac';
				$data['Dat']='23. novembar - 21. decembar';
				break;
			case 'JA': $data['ZnakS']='jarac';
				$data['Dat']='22. decembar - 20. januar';
				break;
			case 'VO': $data['ZnakS']='vodolija';
				$data['Dat']='21. januar - 19. februar';
				break;
			case 'RI': $data['ZnakS']='ribe';
				$data['Dat']='20. februar - 20. mart';
				break;
			}
			//print_r($data);
			if (isset($data['Id'])){
			$data['kom']=$this->Clanak_model->dohvati_komentare($data['Id']);
			//print_r($data['kom']);
			$_SESSION['IdClan']=$data['Id'];}
			else {
				$data['kom']=null;
			$data['IdClan']=null;
		}
			
			//print_r($data['kom']);
			$this->load->view('templates/header');
			if (!(isset($_SESSION['tip'])))
			$this->load->view('gost/header');
			else switch($_SESSION['tip']){
				case 'K':$this->load->view('korisnik/header', $data);break;
				case 'M':
				case 'A':$this->load->view('moderator/header', $data);break;
			}
			$this->load->view('templates/meni');
			if (isset($_SESSION['tip']))
			switch($_SESSION['tip']){
				case 'K':$this->load->view('korisnik/meni');break;
				case 'M':$this->load->view('moderator/meni');break;
				case 'A':$this->load->view('admin/meni');
			}
			$this->load->view('clanak/mesecni',$data);

			$this->load->view('templates/footer');
		}

		public function otvori_osobine($znak){
			session_start();
			$this->load->model('Clanak_model');
			$data=$this->Clanak_model->dohvati_osobine($znak);
			//print_r($data);
			if(isset($_SESSION['tip']) and $_SESSION['tip']=='K'){
				$data['ime'] = $_SESSION['ime'];
				$data['prezime'] = $_SESSION['prezime'];
				$data['tip'] = $_SESSION['tip'];
			}
			else { if (isset($_SESSION['tip']))
				$data['username']=$_SESSION['username'];
			}
			switch($znak){
			case 'OV': $data['ZnakS']='ovan';
				$data['Dat']='21. mart - 20. april';
				break;
			case 'BI': $data['ZnakS']='bik';
				$data['Dat']='21. april - 20. maj';
				break;
			case 'BL':{ $data['ZnakS']='blizanci';
				$data['Dat']='21. maj - 20. jun';
				break;}
			case 'RA': $data['ZnakS']='rak';
				$data['Dat']='21. jun - 20. jul';
				break;
			case 'LA': $data['ZnakS']='lav';
				$data['Dat']='21. jul - 21. avgust';
				break;
			case 'DE': $data['ZnakS']='devica';
				$data['Dat']='22. avgust - 22. septembar';
				break;
			case 'VA': $data['ZnakS']='vaga';
				$data['Dat']='23. septembar - 22. oktobar';
				break;
			case 'SK': $data['ZnakS']='skorpija';
				$data['Dat']='23. oktobar - 22. novembar';
				break;
			case 'ST': $data['ZnakS']='strelac';
				$data['Dat']='23. novembar - 21. decembar';
				break;
			case 'JA': $data['ZnakS']='jarac';
				$data['Dat']='22. decembar - 20. januar';
				break;
			case 'VO': $data['ZnakS']='vodolija';
				$data['Dat']='21. januar - 19. februar';
				break;
			case 'RI': $data['ZnakS']='ribe';
				$data['Dat']='20. februar - 20. mart';
				break;
			}

			if (isset($data['IdClan']))
			$data['kom']=$this->Clanak_model->dohvati_komentare($data['IdClan']);
			else $data['kom']=null;
			//print_r($data);
			$this->load->view('templates/header');
			if (!(isset($_SESSION['tip'])))
			$this->load->view('gost/header');
			else switch($_SESSION['tip']){
				case 'K':$this->load->view('korisnik/header', $data);break;
				case 'M':
				case 'A':$this->load->view('moderator/header', $data);
			}
			$this->load->view('templates/meni');
			if (isset($_SESSION['tip']))
			switch($_SESSION['tip']){
				case 'K':$this->load->view('korisnik/meni');break;
				case 'M':$this->load->view('moderator/meni');break;
				case 'A':$this->load->view('admin/meni');
			}
			$this->load->view('clanak/osobine',$data);
			$this->load->view('templates/footer');
		}

		public function otvori_slaganje($znak){
			session_start();
			$data['Znak']=$znak;
			$this->load->model('Clanak_model');
			$data['niz']=$this->Clanak_model->dohvati_slaganje($znak);
			if(isset($_SESSION['tip']) and $_SESSION['tip']=='K'){
				$data['ime'] = $_SESSION['ime'];
				$data['prezime'] = $_SESSION['prezime'];
				$data['tip'] = $_SESSION['tip'];
			}
			else { if (isset($_SESSION['tip']))
				$data['username']=$_SESSION['username'];
			}
			//print_r($data);
			switch($znak){
			case 'OV': $data['ZnakS']='ovan';
				$data['Dat']='21. mart - 20. april';
				break;
			case 'BI': $data['ZnakS']='bik';
				$data['Dat']='21. april - 20. maj';
				break;
			case 'BL': $data['ZnakS']='blizanci';
				$data['Dat']='21. maj - 20. jun';
				break;
			case 'RA': $data['ZnakS']='rak';
				$data['Dat']='21. jun - 20. jul';
				break;
			case 'LA': $data['ZnakS']='lav';
				$data['Dat']='21. jul - 21. avgust';
				break;
			case 'DE': $data['ZnakS']='devica';
				$data['Dat']='22. avgust - 22. septembar';
				break;
			case 'VA': $data['ZnakS']='vaga';
				$data['Dat']='23. septembar - 22. oktobar';
				break;
			case 'SK': $data['ZnakS']='skorpija';
				$data['Dat']='23. oktobar - 22. novembar';
				break;
			case 'ST': $data['ZnakS']='strelac';
				$data['Dat']='23. novembar - 21. decembar';
				break;
			case 'JA': $data['ZnakS']='jarac';
				$data['Dat']='22. decembar - 20. januar';
				break;
			case 'VO': $data['ZnakS']='vodolija';
				$data['Dat']='21. januar - 19. februar';
				break;
			case 'RI': $data['ZnakS']='ribe';
				$data['Dat']='20. februar - 20. mart';
				break;
			}
			//print_r($data['ZnakS']);
			$this->load->view('templates/header');
			if (!(isset($_SESSION['tip'])))
			$this->load->view('gost/header');
			else switch($_SESSION['tip']){
				case 'K':$this->load->view('korisnik/header', $data);break;
				case 'M':
				case 'A':$this->load->view('moderator/header', $data);
			}
			$this->load->view('templates/meni');
			if (isset($_SESSION['tip']))
			switch($_SESSION['tip']){
				case 'K':$this->load->view('korisnik/meni');break;
				case 'M':$this->load->view('moderator/meni');break;
				case 'A':$this->load->view('admin/meni');
			}

			$this->load->view('clanak/slaganje',$data);

			$this->load->view('templates/footer');
		}



		public function ostavi_komentar(){
			session_start();
			$data['ime']=$_SESSION['ime'];
			$data['prezime']=$_SESSION['prezime'];
			$data['IdAutor']=$_SESSION['id'];
			$data['IdClan']=$_SESSION['IdClan'];
			$data['Tekst']=$this->input->post('komentartekst');
			$data['IdClan']=$_SESSION['IdClan'];
			$this->load->model('Clanak_model');
			$this->Clanak_model->ubaci_komentar($data);
			$data2=$this->Clanak_model->dohvati_zadnji($_SESSION['znakCl']);
			//print_r($data2);
			$data2['ime'] = $_SESSION['ime'];
			$data2['prezime'] = $_SESSION['prezime'];
			$data2['tip'] = $_SESSION['tip'];

			switch($_SESSION['znakCl']){
			case 'OV': $data2['ZnakS']='ovan';
				$data2['Dat']='21. mart - 20. april';
				break;
			case 'BI': $data2['ZnakS']='bik';
				$data2['Dat']='21. april - 20. maj';
				break;
			case 'BL':{ $data2['ZnakS']='blizanci';
				$data2['Dat']='21. maj - 20. jun';
				break;}
			case 'RA': $data2['ZnakS']='rak';
				$data2['Dat']='21. jun - 20. jul';
				break;
			case 'LA': $data2['ZnakS']='lav';
				$data2['Dat']='21. jul - 21. avgust';
				break;
			case 'DE': $data2['ZnakS']='devica';
				$data2['Dat']='22. avgust - 22. septembar';
				break;
			case 'VA': $data2['ZnakS']='vaga';
				$data2['Dat']='23. septembar - 22. oktobar';
				break;
			case 'SK': $data2['ZnakS']='skorpija';
				$data2['Dat']='23. oktobar - 22. novembar';
				break;
			case 'ST': $data2['ZnakS']='strelac';
				$data2['Dat']='23. novembar - 21. decembar';
				break;
			case 'JA': $data2['ZnakS']='jarac';
				$data2['Dat']='22. decembar - 20. januar';
				break;
			case 'VO': $data2['ZnakS']='vodolija';
				$data2['Dat']='21. januar - 19. februar';
				break;
			case 'RI': $data2['ZnakS']='ribe';
				$data2['Dat']='20. februar - 20. mart';
				break;
			}
			if (isset($data2['Id'])){
			$data2['kom']=$this->Clanak_model->dohvati_komentare($data2['Id']);
			$_SESSION['IdClan']=$data2['Id'];}
			else $data2['kom']=null;
			$this->load->view('templates/header');
			$this->load->view('korisnik/header', $data2);	
			$this->load->view('templates/meni');
			$this->load->view('korisnik/meni');
			$this->load->view('clanak/mesecni',$data2);
			$this->load->view('templates/footer');
		}

		public function otvori(){
			session_start();
			$this->load->model('Clanak_model');
			$datak=$this->Clanak_model->dohvati_jedan($this->input->post('Id1'),$this->input->post('Tip1'));
			if ($this->input->post('Tip1')=='S'){
				$data['niz']=$this->Clanak_model->dohvati_slaganje($datak['Znak']);
				$_SESSION['IdClan']=$datak['Id'];
				$_SESSION['Znak']=$datak['Znak'];
				$_SESSION['Tip']=$this->input->post('Tip1');
				switch($datak['Znak']){
			case 'OV': $data['ZnakS']='ovan';
				$data['Dat']='21. mart - 20. april';
				break;
			case 'BI': $data['ZnakS']='bik';
				$data['Dat']='21. april - 20. maj';
				break;
			case 'BL':{ $data['ZnakS']='blizanci';
				$data['Dat']='21. maj - 20. jun';
				break;}
			case 'RA': $data['ZnakS']='rak';
				$data['Dat']='21. jun - 20. jul';
				break;
			case 'LA': $data['ZnakS']='lav';
				$data['Dat']='21. jul - 21. avgust';
				break;
			case 'DE': $data['ZnakS']='devica';
				$data['Dat']='22. avgust - 22. septembar';
				break;
			case 'VA': $data['ZnakS']='vaga';
				$data['Dat']='23. septembar - 22. oktobar';
				break;
			case 'SK': $data['ZnakS']='skorpija';
				$data['Dat']='23. oktobar - 22. novembar';
				break;
			case 'ST': $data['ZnakS']='strelac';
				$data['Dat']='23. novembar - 21. decembar';
				break;
			case 'JA': $data['ZnakS']='jarac';
				$data['Dat']='22. decembar - 20. januar';
				break;
			case 'VO': $data['ZnakS']='vodolija';
				$data['Dat']='21. januar - 19. februar';
				break;
			case 'RI': $data['ZnakS']='ribe';
				$data['Dat']='20. februar - 20. mart';
				break;
			}
			}
			else {
				$data=$datak;
				$data['Id']=$this->input->post('Id1');
				$data['Tip']=$this->input->post('Tip1');
				$_SESSION['IdClan']=$data['Id'];				
				$_SESSION['Znak']=$data['Znak'];
				$_SESSION['Tip']=$data['Tip'];
				switch($data['Znak']){
			case 'OV': $data['ZnakS']='ovan';
				$data['Dat']='21. mart - 20. april';
				break;
			case 'BI': $data['ZnakS']='bik';
				$data['Dat']='21. april - 20. maj';
				break;
			case 'BL':{ $data['ZnakS']='blizanci';
				$data['Dat']='21. maj - 20. jun';
				break;}
			case 'RA': $data['ZnakS']='rak';
				$data['Dat']='21. jun - 20. jul';
				break;
			case 'LA': $data['ZnakS']='lav';
				$data['Dat']='21. jul - 21. avgust';
				break;
			case 'DE': $data['ZnakS']='devica';
				$data['Dat']='22. avgust - 22. septembar';
				break;
			case 'VA': $data['ZnakS']='vaga';
				$data['Dat']='23. septembar - 22. oktobar';
				break;
			case 'SK': $data['ZnakS']='skorpija';
				$data['Dat']='23. oktobar - 22. novembar';
				break;
			case 'ST': $data['ZnakS']='strelac';
				$data['Dat']='23. novembar - 21. decembar';
				break;
			case 'JA': $data['ZnakS']='jarac';
				$data['Dat']='22. decembar - 20. januar';
				break;
			case 'VO': $data['ZnakS']='vodolija';
				$data['Dat']='21. januar - 19. februar';
				break;
			case 'RI': $data['ZnakS']='ribe';
				$data['Dat']='20. februar - 20. mart';
				break;
			}
			}
			$data['username']=$_SESSION['username'];
			
			$this->load->view('templates/header');
			$this->load->view('moderator/header', $data);
			$this->load->view('templates/meni');
			$this->load->view('moderator/meni');
			$data['kom']=null;
			switch($_SESSION['Tip']){
				case 'M':$this->load->view('clanak/edit_mesecni',$data);break;
				case 'S':$this->load->view('clanak/slaganje',$data);break;
				case 'O':$this->load->view('clanak/edit_osobine',$data);break;
			}
			$this->load->view('templates/footer');
		}
		public function obrisi(){
			session_start();
			$this->load->model('Clanak_model');
			$this->Clanak_model->obrisi($this->input->post('Id2'));
			// $data['Id']=$_SESSION['id'];
			// $data['clanci']=$this->Clanak_model->dohvati_moje($data['Id']);

			// $data['username']=$_SESSION['username'];
			// $this->load->view('templates/header');
			// $this->load->view('moderator/header', $data);
			// $this->load->view('templates/meni');
			// $this->load->view('moderator/meni');
			// $this->load->view('templates/mojiclanci',$data);
			// $this->load->view('templates/footer');
			redirect('Moderatori/moji_clanci');
		}
	}


?>