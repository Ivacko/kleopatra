<!--Aleksa Ivacko 2014/0119-->
<?php
	/**
	* Aleksandra Todorovic 0333/2104
	*/
	class Korisnici extends CI_Controller	{
		
		public function index(){
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'K'){
				$data = array(
					'ime' => $_SESSION['ime'],
					'prezime' => $_SESSION['prezime'],
					'kontakti'=>$this->kontakt_model->getKontakti(),
					'blockedusers'=>$this->kontakt_model->getBlokirani(),
					'razlog'=>''
					);
			}
			else{
				redirect('gosti');
			}

			$this->load->view('templates/header');
			$this->load->view('korisnik/header', $data);
			
			$this->load->view('templates/meni');
			$this->load->view('korisnik/meni');
			$this->load->view('templates/index');
		

			$this->load->view('templates/footer');

		}

		public function view($page='index')
		{
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'K'){
				$data = array(
					'ime' => $_SESSION['ime'],
					'prezime' => $_SESSION['prezime'],
					'kontakti'=>$this->kontakt_model->getKontakti(),
					'blockedusers'=>$this->kontakt_model->getBlokirani()

					);
			}
			else{
				redirect('gosti');
			}

			if (file_exists(APPPATH.'views/korisnik/'. $page. '.php'))
			{
				$this->load->view('templates/header');
				$this->load->view('korisnik/header', $data);
				
				$this->load->view('templates/meni');
				$this->load->view('korisnik/meni');
				$this->load->view('korisnik/'.$page);
			

				$this->load->view('templates/footer');
			}
			elseif (file_exists(APPPATH.'views/templates/'. $page. '.php'))
			{
					$this->load->view('templates/header');
					$this->load->view('korisnik/header', $data);
					
					$this->load->view('templates/meni');
					$this->load->view('korisnik/meni');
					$this->load->view('templates/'.$page);
				

					$this->load->view('templates/footer');
			}
			else
			{
				show_404();
			}
			
		}
		public function mojprofil($option = null){
			session_start();
			$data = array(
				'ime' => $_SESSION['ime'],
				'prezime' => $_SESSION['prezime'],
				'email' => $_SESSION['email'],
				'mesto' => $_SESSION['mesto'],
				'biografija' => $_SESSION['biografija'],
				'zanimanja' => $_SESSION['zanimanja'],
				'option' => $option
				);
			
			$this->load->view('templates/header');
			$this->load->view('korisnik/header', $data);
			
			$this->load->view('templates/meni');
			$this->load->view('korisnik/meni');

			if(isset($option)){
				$this->load->view('korisnik/option', $data);
			}
			$this->load->view('korisnik/mojprofil', $data);
			

			$this->load->view('templates/footer');
		}

		public function azuriraj()
		{
			session_start();
			$var = $this->input->post('name');
			if(isset($var) && $_SESSION['ime'] != $var){
				$data['Ime'] = $var;
				$_SESSION['ime'] = $var;
			}
			$var = $this->input->post('surname');
			if(isset($var) && $_SESSION['prezime'] != $var){
				$data['Prezime'] = $var;
				$_SESSION['prezime'] = $var;
			}
			$var = $this->input->post('email');
			if(isset($var) && $_SESSION['email'] != $var){
				$data['Email'] = $var;
				$_SESSION['email'] = $var;
			}
			$var = $this->input->post('place');
			if(isset($var) && $_SESSION['mesto'] != $var){
				$data['Mesto'] = $var;
				$_SESSION['mesto'] = $var;
			}
			$var = $this->input->post('bio');
			if($_SESSION['biografija'] != $var){
				$data['Biografija'] = $var;
				$_SESSION['biografija'] = $var;
			}
			$var = $this->input->post('interes');
			if($_SESSION['zanimanja'] != $var){
				$data['Zanimanja'] = $var;
				$_SESSION['zanimanja'] = $var;
			}
			print_r($_SESSION);

			if(isset($data)){
				$this->load->model('korisnik_model');
				if($this->korisnik_model->updateKorisnik($_SESSION['id'], $data)){
					redirect('korisnici/mojprofil/profil_uspesno_azuriran');
				}
				else{
					redirect('korisnici/mojprofil/greska_pri_azuriranju');
				}
			}
			else
				redirect('korisnici/mojprofil/nista_nije_izmenjeno');
		}

		public function blokiraj($id)
		{	
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'K'){
				$data = array(
					'ime' => $_SESSION['ime'],
					'prezime' => $_SESSION['prezime'],
					'kontakti'=>$this->kontakt_model->getKontakti(),
					'blockedusers'=>$this->kontakt_model->getBlokirani()
					);
			}
			else{
				redirect('gosti');
			}
			$pol=$_SESSION['pol'];
			$this->kontakt_model->blokiraj($id, $pol);
			redirect('korisnik/kontakti');
		}
		public function odblokiraj($idOd)
		{

		
			$this->kontakt_model->odblokiraj($_SESSION['id'], $idOd);
			redirect('korisnik/blokirani');

		}

		public function prijavi()
		{session_start();
			$pol=$_SESSION['pol'];
			$razlog = nl2br($this->input->post('razlog'));

			$this->kontakt_model->prijavi($razlog, $pol);
			redirect('korisnik/kontakti');

		}
		/*Tanja Zivkovic 14/0469*/
		public function ostavi_komentar(){
			session_start();


			$this->load->view('templates/header');
			$this->load->view('korisnik/header', $data);
				
			$this->load->view('templates/meni');
			$this->load->view('korisnik/meni');

			$this->load->view('templates/footer');
		}

	}