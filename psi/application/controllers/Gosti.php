<!--Aleksa Ivacko 2014/0119-->
<?php
	class Gosti extends CI_Controller	{
		
		 // public function __construct()
		 // {
		 //  parent::__construct();
		 //  $this->korisnik = new Korisnik();
		 // }
		
		public function index($option = null){
			session_start();
			if(isset($_SESSION['tip'])){
				if($_SESSION['tip'] == 'A')
					redirect('admini');
				if($_SESSION['tip'] == 'M')
					redirect('moderatori');
				if($_SESSION['tip'] == 'K')
					redirect('korisnici');
			}

			$data = array('option' => $option);
			
			$this->load->view('templates/header');
			$this->load->view('gost/header');

			$this->load->view('templates/meni');
			$this->load->view('templates/index');
			if(isset($option))
				$this->load->view('gost/option', $data);

			$this->load->view('templates/footer');
		
		}

		public function view($page='index')
		{
			if (session_status() == PHP_SESSION_NONE) {
			    session_start();
			}
			if(isset($_SESSION['tip'])){
				if($_SESSION['tip'] == 'K')
					redirect('korisnik/'.$page);
				elseif($_SESSION['tip'] == 'M')
					redirect('moderator/'.$page);
				else 
					redirect('admin/'.$page);

			}

			if (file_exists(APPPATH.'views/gost/'. $page. '.php'))
			{
				$this->load->view('templates/header');
				$this->load->view('gost/header');

				$this->load->view('templates/meni');
				$this->load->view('gost/'.$page);
			

				$this->load->view('templates/footer');
			}
			elseif (file_exists(APPPATH.'views/templates/'. $page. '.php'))
			{
					$this->load->view('templates/header');
					$this->load->view('gost/header');

					$this->load->view('templates/meni');
					$this->load->view('templates/'.$page);
				

					$this->load->view('templates/footer');
			}
			else
			{
				show_404();
			}
			
		}




		public function login(){
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<p><font color="red">', '</font></p>');
			$this->form_validation->set_rules('user', 'Username', 'required|max_length[20]');
			$this->form_validation->set_rules('pass', 'Password', 'required|max_length[20]');
			if($this->form_validation->run() == FALSE){
				$this->index('login');
			}else{
				$data = array(
					'name' => $this->input->post('user'),
					'pass' => $this->input->post('pass'),
					);
				$this->load->model('korisnik_model');
				$row = $this->korisnik_model->getKorisnik($data);
				if (is_null($row)){
					$this->form_validation->reset_validation();
					$this->form_validation->set_rules('user', '', 'invalid', array('invalid' => 'Invalid Username or Password'));
					$this->form_validation->run();
					$this->index('login');
				}
				else{
					if( $row->Tip == 'K'){
						if($this->korisnik_model->isBanned($row->Id)){
							$this->form_validation->reset_validation();
							$this->form_validation->set_rules('user', '', 'banned', array('banned' => 
								'Your account has been banned'));
							$this->form_validation->run();
							$this->index('login');

						}else{
							$rowK = $this->korisnik_model->getRegularan($row->Id);
							session_start();
							$_SESSION['id'] = $row->Id;
							$_SESSION['tip'] = $row->Tip;
							$_SESSION['username'] = $row->Username;
							$_SESSION['pol'] = $rowK->Pol;
							$_SESSION['znak'] = $rowK->Znak;
							$_SESSION['ime'] = $rowK->Ime;
							$_SESSION['prezime'] = $rowK->Prezime;
							$_SESSION['datum'] = $rowK->DatumRodjenja;
							$_SESSION['mesto'] = $rowK->Mesto;
							$_SESSION['email'] = $rowK->Email;
							$_SESSION['biografija'] = $rowK->Biografija;
							$_SESSION['zanimanja'] = $rowK->Zanimanja;
							redirect('korisnici');
						}
					} else{
						$rowM = $this->korisnik_model->getModerator($row->Id);
						if($rowM->Status == 'X' && $rowM->Tip == 'M') {
							$this->form_validation->reset_validation();
							$this->form_validation->set_rules('user', '', 'invalid', array('invalid' => 
								'Moderator is not validated yet'));
							$this->form_validation->run();
							$this->index('login');
						}
						else{
							session_start();
							$_SESSION['id'] = $row->Id;
							$_SESSION['tip'] = $row->Tip;
							$_SESSION['username'] = $row->Username;
							$_SESSION['email'] = $rowM->Email;
							if($row->Tip == 'M')
								redirect('moderatori');
							else
								redirect('admini');
						}
					}
				}
			}
		}

		public function register(){
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<p><font color="red">', '</font></p>');
			$this->load->model('korisnik_model');
			$this->form_validation->set_rules('user', 'Username', 'required|max_length[20]|callback_is_uniq_user');
			$this->form_validation->set_rules('pass', 'Password', 'required|max_length[20]');
			$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|max_length[20]|matches[pass]');
			$this->form_validation->set_rules('email', 'Email', 'required|max_length[20]');
			$this->form_validation->set_rules('name', 'Name', 'required|max_length[20]');
			$this->form_validation->set_rules('surname', 'Surname', 'required|max_length[20]');
			$this->form_validation->set_rules('gender', 'Gender', 'required');
			$this->form_validation->set_rules('date', 'Date of Birth', 'required|callback_date_valid');
			$this->form_validation->set_rules('place', 'Place of Birth', 'required|max_length[20]');
			if($this->form_validation->run() == FALSE){
				$this->index('register');
			}
			else{
				$data = array(
					'user' => $this->input->post('user'),
					'pass' => $this->input->post('pass'),
					'email' => $this->input->post('email'),
					'name' => $this->input->post('name'),
					'surname' => $this->input->post('surname'),
					'date' => $this->input->post('date'),
					'place' => $this->input->post('place'),
					'znak' => $this->getZnak($this->input->post('date')),
					'pol' => $this->input->post('gender')
					);
				if($this->korisnik_model->insertKorisnik($data)){

					redirect('gost/reg_uspesno');

				}
				else{
					$this->form_validation->reset_validation();
					$this->form_validation->set_rules('user', '', 'invalid', array('invalid' => 'Registracija nije uspela, pokusajte kasnije ponovo...'));
					$this->form_validation->run();
					$this->index('register');
				}
			}
		}

		public function registermod(){
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<p><font color="red">', '</font></p>');
			$this->load->model('korisnik_model');
			$this->form_validation->set_rules('user', 'Username', 'required|max_length[20]|callback_is_uniq_user');
			$this->form_validation->set_rules('pass', 'Password', 'required|max_length[20]');
			$this->form_validation->set_rules('passconf', 'Password Confirmation', 'required|max_length[20]|matches[pass]');
			$this->form_validation->set_rules('email', 'Email', 'required|max_length[20]');
			$this->form_validation->set_rules('text', 'Text', 'required');
			if($this->form_validation->run() == FALSE){
				$this->index('registermod');
			}
			else{
				$data = array(
					'user' => $this->input->post('user'),
					'pass' => $this->input->post('pass'),
					'email' => $this->input->post('email'),
					'text' => $this->input->post('text')
					);
				if($this->korisnik_model->insertModerator($data)){
					redirect('gost/reg_uspesno');

				}
				else{
					$this->form_validation->reset_validation();
					$this->form_validation->set_rules('user', '', 'invalid', array('invalid' => 'Registracija nije uspela, pokusajte kasnije ponovo...'));
					$this->form_validation->run();
					$this->index('registermod');
				}
			}
		}


		public function signout(){
			session_start();
			session_destroy();

			redirect('gost/signout');
		}

		public function odredi_znak(){
			session_start();
			
			$this->load->library('form_validation');
			$this->form_validation->set_error_delimiters('<p><b>', '</b></p>');

			$date = $this->input->post('date');
			$this->form_validation->set_rules('date', '', 'required', array('required' => 'Niste uneli datum'));
			if(!($date === ""))
				if($this->check_date($date) == false){
					$this->form_validation->set_rules('date', '', 'poruka', array('poruka' => 'Datum je loseg formata'));
				}
				else{
					$znak = $this->expandZnak($this->getZnak($date));
					$this->form_validation->set_rules('date', '', 'poruka', array('poruka' => 'Vas znak je: <a href="'.base_url().'clanak/otvori_osobine/'.$this->getZnak($date).'">'.$znak.'</a>'));
				}
			$this->form_validation->run();

			$this->load->view('templates/header');
			if(isset($_SESSION['tip'])){
				if($_SESSION['tip'] == 'K'){
					$data['ime'] = $_SESSION['ime'];
					$data['prezime'] = $_SESSION['prezime'];
					$this->load->view('korisnik/header', $data);
					$this->load->view('templates/meni');
					$this->load->view('korisnik/meni');
				}
				elseif($_SESSION['tip'] == 'M'){
					$data['username'] = $_SESSION['username'];
					$this->load->view('moderator/header', $data);
					$this->load->view('templates/meni');
					$this->load->view('moderator/meni');
				}
				else {
					$data['username'] = $_SESSION['username'];
					$this->load->view('moderator/header', $data);
					$this->load->view('templates/meni');
					$this->load->view('admin/meni');
				}
			}
			else{
				$this->load->view('gost/header');
				$this->load->view('templates/meni');
			}
			$this->load->view('templates/odredi_znak');
			$this->load->view('templates/footer');
			

		}

		public function is_uniq_user($user){
			$this->form_validation->set_message('is_uniq_user', 'The {field} is not unique.');
			return $this->korisnik_model->isUniqueUsername($user);
		}

		function date_valid($date){
		    $year = (int) substr($date, 0, 4);
		    $month = (int) substr($date, 5, 2);
		    $day = (int) substr($date, 8, 2);
		    $this->form_validation->set_message('date_valid', 'The {field} doesn\'have a correct format.');
		    return checkdate($month, $day, $year);
		}

		function check_date($date){
			$year = (int) substr($date, 0, 4);
		    $month = (int) substr($date, 5, 2);
		    $day = (int) substr($date, 8, 2);
		    return checkdate($month, $day, $year);

		}

		public function getZnak($date){
			$month = (int) substr($date, 5, 2);
		    $day = (int) substr($date, 8, 2);
		    if($month < 2 && $day < 21)
		    	return 'JA';
		    elseif($month == 1 || ($month < 3 && $day < 20))
		    	return 'VO';
		    elseif($month == 2 || ($month < 4 && $day < 22))
		    	return 'RI';
		    elseif($month == 3 || ($month < 5 && $day < 21))
		    	return 'OV';
		   	elseif($month == 4 || ($month < 6 && $day < 22))
		    	return 'BI';
		   	elseif($month == 5 || ($month < 7 && $day < 22))
		    	return 'BL';
		   	elseif($month == 6 || ($month < 8 && $day < 24))
			   	return 'RA';
		   	elseif($month == 7 || ($month < 9 && $day < 24))
		    	return 'LA';
		   	elseif($month == 8 || ($month < 10 && $day < 24))
		    	return 'DE';
		   	elseif($month == 9 || ($month < 11 && $day < 24))
		    	return 'VA';
		   	elseif($month == 10 || ($month < 12 && $day < 23))
		    	return 'SK';
	    	elseif($month == 11 || $day < 23)
		    	return 'ST';
	    	else 
		    	return 'JA';
		}

		public function expandZnak($znak){

			switch($znak){
			case 'OV': $data ='Ovan';
				
				break;
			case 'BI': $data ='Bik';
			
				break;
			case 'BL': $data ='Blizanci';
				
				break;
			case 'RA': $data='Rak';
				
				break;
			case 'LA': $data='Lav';
				
				break;
			case 'DE': $data='Devica';
				
				break;
			case 'VA': $data='Vaga';
			
				break;
			case 'SK': $data='Skorpija';
				
				break;
			case 'ST': $data='Strelac';
				
				break;
			case 'JA': $data='Jarac';
				
				break;
			case 'VO': $data='Vodolija';
			
				break;
			case 'RI': $data='Ribe';
				
				break;

			}
			return $data;
		}

		

	}