<?php
	
	/**
	* Aleksandra Todorovic 0333/2104
	*/
	class Admini extends CI_Controller	{

		public function index(){
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'A'){
				$data = array(
					'username' => $_SESSION['username'],
					'komentari'=> $this->komentari_model->getNeobjavljeni('X'),
					'listamod'=>$this->korisnik_model->getModeratori('O'),
					'odobrimod'=>$this->korisnik_model->getModeratori('X'),
					'prijavekor'=>$this->korisnik_model->getPrijave(),
					'prijavekor2'=>$this->korisnik_model->getPrijave2()
					);
			}
			else{
				redirect('gosti');
			}

			$this->load->view('templates/header');
			$this->load->view('moderator/header', $data);
			
			$this->load->view('templates/meni');
			$this->load->view('admin/meni');
			$this->load->view('templates/index');
		

			$this->load->view('templates/footer');

		}
		
		public function view($page='index')
		{
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'A'){
				$data = array(
					
					'username' => $_SESSION['username'],
					'komentari'=> $this->komentari_model->getNeobjavljeni('X'),
					'listamod'=>$this->korisnik_model->getModeratori('O'),
					'odobrimod'=>$this->korisnik_model->getModeratori('X'),
					'prijavekor'=>$this->korisnik_model->getPrijave(),
					'prijavekor2'=>$this->korisnik_model->getPrijave2()
					);
			}
			else{
				redirect('gosti');
			}
			
			if (file_exists(APPPATH.'views/admin/'. $page. '.php'))
			{
				$this->load->view('templates/header');
				$this->load->view('moderator/header', $data);

				$this->load->view('templates/meni');
				$this->load->view('admin/meni');
				$this->load->view('admin/'.$page, $data);
			

				$this->load->view('templates/footer');
				
			}
			elseif (file_exists(APPPATH.'views/templates/'. $page. '.php'))
			{
					$this->load->view('templates/header');
					$this->load->view('moderator/header', $data);

					$this->load->view('templates/meni');
					$this->load->view('admin/meni');
					$this->load->view('templates/'.$page, $data);
				

					$this->load->view('templates/footer');
					
			}
			else
			{
				show_404();
			}
			
		}

		public function izbrisiKomentar()
		{
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'A'){
				$data = array(
					'username' => $_SESSION['username'],
					'komentari'=> $this->komentari_model->getNeobjavljeni('X'),
					'listamod'=>$this->korisnik_model->getModeratori('O'),
					'odobrimod'=>$this->korisnik_model->getModeratori('X'),
					'prijavekor'=>$this->korisnik_model->getPrijave(),
					'prijavekor2'=>$this->korisnik_model->getPrijave2()
					);
			}
			else{
				redirect('gosti');
			}
			$this->komentari_model->izbrisiKomentar();
			redirect('admin/komentari');
		}
		public function objaviKomentar()
		{
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'A'){
				$data = array(
					'username' => $_SESSION['username'],
					'komentari'=> $this->komentari_model->getNeobjavljeni('X'),
					'listamod'=>$this->korisnik_model->getModeratori('O'),
					'odobrimod'=>$this->korisnik_model->getModeratori('X'),
					'prijavekor'=>$this->korisnik_model->getPrijave(),
					'prijavekor2'=>$this->korisnik_model->getPrijave2()
					);
			}
			else{
				redirect('gosti');
			}
			$q=$this->komentari_model->objaviKomentar();
			redirect('admin/komentari');
			
		}
		public function izbrisiModeratora($id)
		{
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'A'){
				$data = array(
					'username' => $_SESSION['username'],
					'komentari'=> $this->komentari_model->getNeobjavljeni('X'),
					'listamod'=>$this->korisnik_model->getModeratori('O'),
					'odobrimod'=>$this->korisnik_model->getModeratori('X'),
					'prijavekor'=>$this->korisnik_model->getPrijave(),
					'prijavekor2'=>$this->korisnik_model->getPrijave2()
					);
			}
			else{
				redirect('gosti');
			}
			$this->korisnik_model->izbrisiModeratora($id);
			redirect('admin/listamoderatora');
		}

		public function odobriModeratora()
		{
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'A'){
				$data = array(
					'username' => $_SESSION['username'],
					'komentari'=> $this->komentari_model->getNeobjavljeni('X'),
					'listamod'=>$this->korisnik_model->getModeratori('O'),
					'odobrimod'=>$this->korisnik_model->getModeratori('X'),
					'prijavekor'=>$this->korisnik_model->getPrijave(),
					'prijavekor2'=>$this->korisnik_model->getPrijave2()
					);
			}
			else{
				redirect('gosti');
			}
			$q=$this->korisnik_model->odobriModeratora();
			redirect('admin/prijavemod');
		}
		public function izbrisiPrijavu($id)
		{
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'A'){
				$data = array(
					'username' => $_SESSION['username'],
					'komentari'=> $this->komentari_model->getNeobjavljeni('X'),
					'listamod'=>$this->korisnik_model->getModeratori('O'),
					'odobrimod'=>$this->korisnik_model->getModeratori('X'),
					'prijavekor'=>$this->korisnik_model->getPrijave(),
					'prijavekor2'=>$this->korisnik_model->getPrijave2()
					);
			}
			else{
				redirect('gosti');
			}
			$this->korisnik_model->izbrisiModeratora($id);
			redirect('admin/prijavemod');
		}

		public function Banuj8()
		{
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'A'){
				$data = array(
					'username' => $_SESSION['username'],
					'komentari'=> $this->komentari_model->getNeobjavljeni('X'),
					'listamod'=>$this->korisnik_model->getModeratori('O'),
					'odobrimod'=>$this->korisnik_model->getModeratori('X'),
					'prijavekor'=>$this->korisnik_model->getPrijave(),
					'prijavekor2'=>$this->korisnik_model->getPrijave2()
					);
			}
			else{
				redirect('gosti');
			}
			$q=$this->korisnik_model->ban8();
			redirect('admin/prijave');
		}
		public function Banuj24()
		{
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'A'){
				$data = array(
					'username' => $_SESSION['username'],
					'komentari'=> $this->komentari_model->getNeobjavljeni('X'),
					'listamod'=>$this->korisnik_model->getModeratori('O'),
					'odobrimod'=>$this->korisnik_model->getModeratori('X'),
					'prijavekor'=>$this->korisnik_model->getPrijave(),
					'prijavekor2'=>$this->korisnik_model->getPrijave2()
					);
			}
			else{
				redirect('gosti');
			}
			$q=$this->korisnik_model->ban24();
			redirect('admin/prijave');
		}
		public function Banuj72()
		{
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'A'){
				$data = array(
					'username' => $_SESSION['username'],
					'komentari'=> $this->komentari_model->getNeobjavljeni('X'),
					'listamod'=>$this->korisnik_model->getModeratori('O'),
					'odobrimod'=>$this->korisnik_model->getModeratori('X'),
					'prijavekor'=>$this->korisnik_model->getPrijave(),
					'prijavekor2'=>$this->korisnik_model->getPrijave2()
					);
			}
			else{
				redirect('gosti');
			}
			$q=$this->korisnik_model->ban72();
			redirect('admin/prijave');
		}
		public function Banuj()
		{
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'A'){
				$data = array(
					'username' => $_SESSION['username'],
					'komentari'=> $this->komentari_model->getNeobjavljeni('X'),
					'listamod'=>$this->korisnik_model->getModeratori('O'),
					'odobrimod'=>$this->korisnik_model->getModeratori('X'),
					'prijavekor'=>$this->korisnik_model->getPrijave(),
					'prijavekor2'=>$this->korisnik_model->getPrijave2()
					);
			}
			else{
				redirect('gosti');
			}
			$q=$this->korisnik_model->ban();
			redirect('admin/prijave');
		}
		public function izbrisiKorPrijava($id)
		{
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'A'){
				$data = array(
					'username' => $_SESSION['username'],
					'komentari'=> $this->komentari_model->getNeobjavljeni('X'),
					'listamod'=>$this->korisnik_model->getModeratori('O'),
					'odobrimod'=>$this->korisnik_model->getModeratori('X'),
					'prijavekor'=>$this->korisnik_model->getPrijave(),
					'prijavekor2'=>$this->korisnik_model->getPrijave2()
					);
			}
			else{
				redirect('gosti');
			}
			$this->korisnik_model->izbrisiKorPrijava($id);
			redirect('admin/prijave');
		}

		public function skiniBan($id)
		{
			session_start();

			if(isset($_SESSION['tip']) && $_SESSION['tip'] == 'A'){
				$data = array(
					'username' => $_SESSION['username'],
					'komentari'=> $this->komentari_model->getNeobjavljeni('X'),
					'listamod'=>$this->korisnik_model->getModeratori('O'),
					'odobrimod'=>$this->korisnik_model->getModeratori('X'),
					'prijavekor'=>$this->korisnik_model->getPrijave(),
					'prijavekor2'=>$this->korisnik_model->getPrijave2()
					);
			}
			else{
				redirect('gosti');
			}
			$this->korisnik_model->izbrisiKorPrijava($id);
			redirect('admin/banovani');
		}

		/*Tanja Zivkovic 14/0469*/
		public function napisi_clanak(){
			session_start();
			$data['username']=$_SESSION['username'];
			$this->load->library('form_validation');
			$this->form_validation->set_rules('tip_cl', 'Tip clanka', 'required');
			$this->form_validation->set_rules('znak', 'znak', 'required');
			if($this->form_validation->run() == FALSE){
				$this->napisi_clanak();
			}else{
			$data['Tip']=$this->input->post('tip_cl');
			$data['Znak']=$this->input->post('znak');
			$_SESSION['Tip']=$data['Tip'];
			$_SESSION['Znak']=$data['Znak'];
			switch($data['Znak']){
			case 'OV': $data['ZnakS']='ovan';
				$data['Dat']='21. mart - 20. april';
				break;
			case 'BI': $data['ZnakS']='bik';
				$data['Dat']='21. april - 20. maj';
				break;
			case 'BL':{ $data['ZnakS']='blizanci';
				$data['Dat']='21. maj - 20. jun';
				break;}
			case 'RA': $data['ZnakS']='rak';
				$data['Dat']='21. jun - 20. jul';
				break;
			case 'LA': $data['ZnakS']='lav';
				$data['Dat']='21. jul - 21. avgust';
				break;
			case 'DE': $data['ZnakS']='devica';
				$data['Dat']='22. avgust - 22. septembar';
				break;
			case 'VA': $data['ZnakS']='vaga';
				$data['Dat']='23. septembar - 22. oktobar';
				break;
			case 'SK': $data['ZnakS']='skorpija';
				$data['Dat']='23. oktobar - 22. novembar';
				break;
			case 'ST': $data['ZnakS']='strelac';
				$data['Dat']='23. novembar - 21. decembar';
				break;
			case 'JA': $data['ZnakS']='jarac';
				$data['Dat']='22. decembar - 20. januar';
				break;
			case 'VO': $data['ZnakS']='vodolija';
				$data['Dat']='21. januar - 19. februar';
				break;
			case 'RI': $data['ZnakS']='ribe';
				$data['Dat']='20. februar - 20. mart';
				break;
			}
			$_SESSION['ZnakS']=$data['ZnakS'];
			switch($data['Tip']){
				case 'M':
					$data['Ljubav']="";
					$data['Posao']="";
					$data['Zdravlje']="";
					$this->load->view('templates/header');
					$this->load->view('moderator/header', $data);
					$this->load->view('templates/meni');
					If($_SESSION['tip']=='M')
					$this->load->view('moderator/meni');
					else $this->load->view('admin/meni');
					$this->load->view('templates/napisi_mesecni', $data);
					$this->load->view('templates/footer');
						break;
				case 'O':
					$data['Tekst']="";
					$this->load->view('templates/header');
					$this->load->view('moderator/header', $data);
					$this->load->view('templates/meni');
					If($_SESSION['tip']=='M')
					$this->load->view('moderator/meni');
					else $this->load->view('admin/meni');
					$this->load->view('templates/napisi_osobine', $data);
					$this->load->view('templates/footer');
					break;
				case 'S':

					$this->load->view('templates/header');
					$this->load->view('moderator/header', $data);
					$this->load->view('templates/meni');
					If($_SESSION['tip']=='M')
					$this->load->view('moderator/meni');
					else $this->load->view('admin/meni');
					$this->load->view('templates/napisi_slaganje', $data);
					$this->load->view('templates/footer');
				}
			
			}
		}

	}