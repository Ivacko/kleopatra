-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2017 at 10:16 AM
-- Server version: 5.7.17-log
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kleopatra`
--

-- --------------------------------------------------------

--
-- Table structure for table `autori`
--

CREATE TABLE `autori` (
  `IdMod` int(11) NOT NULL,
  `IdClan` int(11) NOT NULL,
  `Datum` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `autori`
--
DELIMITER $$
CREATE TRIGGER `incBrNapisanih` BEFORE INSERT ON `autori` FOR EACH ROW update moderator
set BrNapisanih = BrNapisanih + 1
where Id = NEW.IdMod
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `blokirani`
--

CREATE TABLE `blokirani` (
  `IdOd` int(11) NOT NULL,
  `IdZa` int(11) NOT NULL,
  `Ime` varchar(20) DEFAULT NULL,
  `Prezime` varchar(20) DEFAULT NULL,
  `DatumOd` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `blokirani`
--
DELIMITER $$
CREATE TRIGGER `addImeNaBlokirani` BEFORE INSERT ON `blokirani` FOR EACH ROW BEGIN
	declare vIme varchar(20); 
    declare vPrezime varchar(20);
    
    select Ime, Prezime into vIme, vPrezime
    from regularan
    where Id = NEW.IdZa;
    
    set NEW.Ime = vIme;
    set NEW.Prezime = vPrezime;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `ch_clanak`
-- (See below for the actual view)
--
CREATE TABLE `ch_clanak` (
`Tip` char(2)
,`Znak` char(3)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `ch_komentar`
-- (See below for the actual view)
--
CREATE TABLE `ch_komentar` (
`Status` char(2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `ch_kontakti`
-- (See below for the actual view)
--
CREATE TABLE `ch_kontakti` (
`OcenaM` int(11)
,`OcenaZ` int(11)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `ch_korisnik`
-- (See below for the actual view)
--
CREATE TABLE `ch_korisnik` (
`Tip` char(2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `ch_mesecni`
-- (See below for the actual view)
--
CREATE TABLE `ch_mesecni` (
`Tip` char(2)
,`Znak` char(3)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `ch_moderator`
-- (See below for the actual view)
--
CREATE TABLE `ch_moderator` (
`Tip` char(2)
,`Status` char(2)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `ch_regularan`
-- (See below for the actual view)
--
CREATE TABLE `ch_regularan` (
`Tip` char(2)
,`Pol` char(2)
,`Znak` char(3)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `ch_slaganje`
-- (See below for the actual view)
--
CREATE TABLE `ch_slaganje` (
`Tip` char(2)
,`ZnakOd` char(3)
,`ZnakZa` char(3)
);

-- --------------------------------------------------------

--
-- Table structure for table `clanak`
--

CREATE TABLE `clanak` (
  `Id` int(11) NOT NULL,
  `Tip` char(2) NOT NULL,
  `Znak` char(3) NOT NULL,
  `DatumOd` date NOT NULL,
  `Tekst` longtext,
  `IdRezervator` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `clanak`
--
DELIMITER $$
CREATE TRIGGER `changeBrRezervisanih` AFTER UPDATE ON `clanak` FOR EACH ROW if old.IdRezervator is null and new.IdRezervator is not null then
	update moderator
    set BrRezervisanih = BrRezervisanih + 1
    where Id = new.IdRezervator;
elseif new.IdRezervator is null and old.IdRezervator is not null then
	update moderator
    set BrRezervisanih = BrRezervisanih - 1
    where Id = old.IdRezervator;
end if
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `Id` int(11) NOT NULL,
  `IdAutor` int(11) NOT NULL,
  `IdMod` int(11) DEFAULT NULL,
  `Datum` date NOT NULL,
  `Status` char(2) NOT NULL DEFAULT 'X',
  `Tekst` longtext NOT NULL,
  `IdClan` int(11) NOT NULL,
  `Ime` varchar(20) DEFAULT NULL,
  `Prezime` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `komentar`
--
DELIMITER $$
CREATE TRIGGER `dodajIme` BEFORE INSERT ON `komentar` FOR EACH ROW BEGIN
	declare vIme varchar(20); 
    declare vPrezime varchar(20);
    
    select Ime, Prezime into vIme, vPrezime
    from regularan
    where Id = NEW.IdAutor;
    
    set NEW.Ime = vIme;
    set NEW.Prezime = vPrezime;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `incBrKomentara` AFTER UPDATE ON `komentar` FOR EACH ROW if old.IdMod is null and new.IdMod is not null then
	update moderator
    set BrKomentara = BrKomentara + 1
    where Id = new.IdMod;
end if
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `kontakti`
--

CREATE TABLE `kontakti` (
  `IdM` int(11) NOT NULL,
  `IdZ` int(11) NOT NULL,
  `OcenaM` int(11) NOT NULL DEFAULT '3',
  `OcenaZ` int(11) NOT NULL DEFAULT '3',
  `DatumOd` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `kontakti`
--
DELIMITER $$
CREATE TRIGGER `decBrKontakta` BEFORE DELETE ON `kontakti` FOR EACH ROW BEGIN
	update regularan
    set BrKontakta = BrKontakta - 1
    where Id = OLD.IdM or Id = OLD.IdZ;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `incBrojKontakta` AFTER INSERT ON `kontakti` FOR EACH ROW BEGIN
	update regularan
    set BrKontakta = BrKontakta + 1
    where Id = NEW.IdM or Id = NEW.IdZ;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `updateSlaganje` AFTER UPDATE ON `kontakti` FOR EACH ROW BEGIN
    declare vZnakM char(3);
    declare vZnakZ char(3);
    select Znak into vZnakM from regularan
    where Id = new.IdM;
    select Znak into vZnakZ from regularan
    where Id = new.IdZ;
    
    if old.OcenaM <> new.OcenaM THEN
    	if old.OcenaM >= 3 then
            update slaganje
            SET OcenaPoz = OcenaPoz - (old.OcenaM - 3)
            where ZnakOd = vZnakM and ZnakZa = vZnakZ;
        else
        	update slaganje
            SET OcenaNeg = OcenaNeg - (3 - old.OcenaM)
            where ZnakOd = vZnakM and ZnakZa = vZnakZ;
        end if;
        if new.OcenaM >= 3 then
            update slaganje
            SET OcenaPoz = OcenaPoz + (new.OcenaM - 3)
            where ZnakOd = vZnakM and ZnakZa = vZnakZ;
        else
        	update slaganje
            SET OcenaNeg = OcenaNeg + (3 - new.OcenaM)
            where ZnakOd = vZnakM and ZnakZa = vZnakZ;
        end if;
    end if;
    
    if old.OcenaZ <> new.OcenaZ THEN
    	if old.OcenaZ >= 3 then
            update slaganje
            SET OcenaPoz = OcenaPoz - old.OcenaZ + 3
            where ZnakOd = vZnakZ and ZnakZa = vZnakM;
        else
        	update slaganje
            SET OcenaNeg = OcenaNeg + old.OcenaZ - 3
            where ZnakOd = vZnakZ and ZnakZa = vZnakM;
        end if;
        if new.OcenaZ >= 3 then
            update slaganje
            SET OcenaPoz = OcenaPoz + new.OcenaZ - 3
            where ZnakOd = vZnakZ and ZnakZa = vZnakM;
        else
        	update slaganje
            SET OcenaNeg = OcenaNeg - new.OcenaZ + 3
            where ZnakOd = vZnakZ and ZnakZa = vZnakM;
        end if;
    end if;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `korisnik`
--

CREATE TABLE `korisnik` (
  `Id` int(11) NOT NULL,
  `Tip` char(2) NOT NULL,
  `Username` varchar(20) NOT NULL,
  `Password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mesecni`
--

CREATE TABLE `mesecni` (
  `Id` int(11) NOT NULL,
  `Tip` char(2) NOT NULL DEFAULT 'M',
  `Znak` char(3) NOT NULL,
  `Ljubav` longtext NOT NULL,
  `Posao` longtext NOT NULL,
  `Zdravlje` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `moderator`
--

CREATE TABLE `moderator` (
  `Id` int(11) NOT NULL,
  `Tip` char(2) NOT NULL DEFAULT 'M',
  `Email` varchar(20) NOT NULL,
  `BrZabrana` int(11) NOT NULL DEFAULT '0',
  `BrKomentara` int(11) NOT NULL DEFAULT '0',
  `BrNapisanih` int(11) NOT NULL DEFAULT '0',
  `BrRezervisanih` int(11) NOT NULL DEFAULT '0',
  `Opis` longtext NOT NULL,
  `Status` char(2) NOT NULL DEFAULT 'X'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `regularan`
--

CREATE TABLE `regularan` (
  `Id` int(11) NOT NULL,
  `Tip` char(2) NOT NULL DEFAULT 'K',
  `Pol` char(2) NOT NULL,
  `Znak` char(3) NOT NULL,
  `Ime` varchar(20) NOT NULL,
  `Prezime` varchar(20) NOT NULL,
  `DatumRodjenja` date NOT NULL,
  `Mesto` varchar(20) NOT NULL,
  `Email` varchar(20) NOT NULL,
  `Biografija` longtext,
  `Zanimanja` longtext,
  `BrKontakta` int(11) NOT NULL DEFAULT '0',
  `BrZahteva` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `regularan`
--
DELIMITER $$
CREATE TRIGGER `changeIme` AFTER UPDATE ON `regularan` FOR EACH ROW BEGIN
    if old.Ime <> new.Ime then 
        update blokirani
        set Ime = new.Ime
        where IdZa = new.Id;
        update zahtev
        set Ime = new.Ime
        where IdZa = new.Id;
        update komentar
        set Ime = new.Ime
        where IdAutor = new.Id;
    end if;
    if old.Prezime <> new.Prezime then 
        update blokirani
        set Prezime = new.Prezime
        where IdZa = new.Id;
        update zahtev
        set Prezime = new.Prezime
        where IdZa = new.Id;
        update komentar
        set Prezime = new.Prezime
        where IdAutor = new.Id;
    end if;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `slaganje`
--

CREATE TABLE `slaganje` (
  `Id` int(11) NOT NULL,
  `Tip` char(2) NOT NULL DEFAULT 'S',
  `ZnakOd` char(3) NOT NULL,
  `ZnakZa` char(3) NOT NULL,
  `OcenaPoz` int(11) NOT NULL,
  `OcenaNeg` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `zabrane`
--

CREATE TABLE `zabrane` (
  `Id` int(11) NOT NULL,
  `IdKor` int(11) NOT NULL,
  `IdMod` int(11) DEFAULT NULL,
  `DatumOd` date DEFAULT NULL,
  `DatumDo` date DEFAULT NULL,
  `Razlog` longtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `zabrane`
--
DELIMITER $$
CREATE TRIGGER `incBrZabrana` AFTER UPDATE ON `zabrane` FOR EACH ROW if old.IdMod is null and new.IdMod is not null then
	update moderator
    set BrZabrana = BrZabrana + 1
    where Id = new.IdMod;
end if
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `zahtev`
--

CREATE TABLE `zahtev` (
  `IdOd` int(11) NOT NULL,
  `IdZa` int(11) NOT NULL,
  `Ime` varchar(20) DEFAULT NULL,
  `Prezime` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Triggers `zahtev`
--
DELIMITER $$
CREATE TRIGGER `decBrZahteva` AFTER DELETE ON `zahtev` FOR EACH ROW BEGIN
	update regularan
    set BrZahteva = BrZahteva - 1
    where Id = old.IdZa;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `dodajImeNaZahtev` BEFORE INSERT ON `zahtev` FOR EACH ROW BEGIN
	declare vIme varchar(20); 
    declare vPrezime varchar(20);
    
    select Ime, Prezime into vIme, vPrezime
    from regularan
    where Id = NEW.IdOd;
    
    set NEW.Ime = vIme;
    set NEW.Prezime = vPrezime;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `incBrZahteva` BEFORE INSERT ON `zahtev` FOR EACH ROW BEGIN
	update regularan
    set BrZahteva = BrZahteva + 1
    where Id = NEW.IdZa;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Structure for view `ch_clanak`
--
DROP TABLE IF EXISTS `ch_clanak`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ch_clanak`  AS  select `clanak`.`Tip` AS `Tip`,`clanak`.`Znak` AS `Znak` from `clanak` where (((`clanak`.`Tip` = 'M') or (`clanak`.`Tip` = 'S') or (`clanak`.`Tip` = 'Z')) and ((`clanak`.`Znak` = 'OV') or (`clanak`.`Znak` = 'BI') or (`clanak`.`Znak` = 'BL') or (`clanak`.`Znak` = 'RA') or (`clanak`.`Znak` = 'LA') or (`clanak`.`Znak` = 'DE') or (`clanak`.`Znak` = 'VA') or (`clanak`.`Znak` = 'SK') or (`clanak`.`Znak` = 'ST') or (`clanak`.`Znak` = 'JA') or (`clanak`.`Znak` = 'VO') or (`clanak`.`Znak` = 'RI'))) WITH CASCADED CHECK OPTION ;

-- --------------------------------------------------------

--
-- Structure for view `ch_komentar`
--
DROP TABLE IF EXISTS `ch_komentar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ch_komentar`  AS  select `komentar`.`Status` AS `Status` from `komentar` where ((`komentar`.`Status` = 'X') or (`komentar`.`Status` = 'O')) WITH CASCADED CHECK OPTION ;

-- --------------------------------------------------------

--
-- Structure for view `ch_kontakti`
--
DROP TABLE IF EXISTS `ch_kontakti`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ch_kontakti`  AS  select `kontakti`.`OcenaM` AS `OcenaM`,`kontakti`.`OcenaZ` AS `OcenaZ` from `kontakti` where ((`kontakti`.`OcenaM` > 0) and (`kontakti`.`OcenaM` < 6) and (`kontakti`.`OcenaZ` > 0) and (`kontakti`.`OcenaZ` < 6)) WITH CASCADED CHECK OPTION ;

-- --------------------------------------------------------

--
-- Structure for view `ch_korisnik`
--
DROP TABLE IF EXISTS `ch_korisnik`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ch_korisnik`  AS  select `korisnik`.`Tip` AS `Tip` from `korisnik` where ((`korisnik`.`Tip` = 'K') or (`korisnik`.`Tip` = 'M') or (`korisnik`.`Tip` = 'A')) WITH CASCADED CHECK OPTION ;

-- --------------------------------------------------------

--
-- Structure for view `ch_mesecni`
--
DROP TABLE IF EXISTS `ch_mesecni`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ch_mesecni`  AS  select `mesecni`.`Tip` AS `Tip`,`mesecni`.`Znak` AS `Znak` from `mesecni` where ((`mesecni`.`Tip` = 'M') and ((`mesecni`.`Znak` = 'OV') or (`mesecni`.`Znak` = 'BI') or (`mesecni`.`Znak` = 'BL') or (`mesecni`.`Znak` = 'RA') or (`mesecni`.`Znak` = 'LA') or (`mesecni`.`Znak` = 'DE') or (`mesecni`.`Znak` = 'VA') or (`mesecni`.`Znak` = 'SK') or (`mesecni`.`Znak` = 'ST') or (`mesecni`.`Znak` = 'JA') or (`mesecni`.`Znak` = 'VO') or (`mesecni`.`Znak` = 'RI'))) WITH CASCADED CHECK OPTION ;

-- --------------------------------------------------------

--
-- Structure for view `ch_moderator`
--
DROP TABLE IF EXISTS `ch_moderator`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ch_moderator`  AS  select `moderator`.`Tip` AS `Tip`,`moderator`.`Status` AS `Status` from `moderator` where (((`moderator`.`Tip` = 'M') or (`moderator`.`Tip` = 'O')) and ((`moderator`.`Status` = 'X') or (`moderator`.`Status` = 'O'))) WITH CASCADED CHECK OPTION ;

-- --------------------------------------------------------

--
-- Structure for view `ch_regularan`
--
DROP TABLE IF EXISTS `ch_regularan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ch_regularan`  AS  select `regularan`.`Tip` AS `Tip`,`regularan`.`Pol` AS `Pol`,`regularan`.`Znak` AS `Znak` from `regularan` where ((`regularan`.`Tip` = 'K') and ((`regularan`.`Pol` = 'M') or (`regularan`.`Pol` = 'Z')) and ((`regularan`.`Znak` = 'OV') or (`regularan`.`Znak` = 'BI') or (`regularan`.`Znak` = 'BL') or (`regularan`.`Znak` = 'RA') or (`regularan`.`Znak` = 'LA') or (`regularan`.`Znak` = 'DE') or (`regularan`.`Znak` = 'VA') or (`regularan`.`Znak` = 'SK') or (`regularan`.`Znak` = 'ST') or (`regularan`.`Znak` = 'JA') or (`regularan`.`Znak` = 'VO') or (`regularan`.`Znak` = 'RI'))) WITH CASCADED CHECK OPTION ;

-- --------------------------------------------------------

--
-- Structure for view `ch_slaganje`
--
DROP TABLE IF EXISTS `ch_slaganje`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `ch_slaganje`  AS  select `slaganje`.`Tip` AS `Tip`,`slaganje`.`ZnakOd` AS `ZnakOd`,`slaganje`.`ZnakZa` AS `ZnakZa` from `slaganje` where ((`slaganje`.`Tip` = 'S') and ((`slaganje`.`ZnakOd` = 'OV') or (`slaganje`.`ZnakOd` = 'BI') or (`slaganje`.`ZnakOd` = 'BL') or (`slaganje`.`ZnakOd` = 'RA') or (`slaganje`.`ZnakOd` = 'LA') or (`slaganje`.`ZnakOd` = 'DE') or (`slaganje`.`ZnakOd` = 'VA') or (`slaganje`.`ZnakOd` = 'SK') or (`slaganje`.`ZnakOd` = 'ST') or (`slaganje`.`ZnakOd` = 'JA') or (`slaganje`.`ZnakOd` = 'VO') or (`slaganje`.`ZnakOd` = 'RI')) and ((`slaganje`.`ZnakZa` = 'OV') or (`slaganje`.`ZnakZa` = 'BI') or (`slaganje`.`ZnakZa` = 'BL') or (`slaganje`.`ZnakZa` = 'RA') or (`slaganje`.`ZnakZa` = 'LA') or (`slaganje`.`ZnakZa` = 'DE') or (`slaganje`.`ZnakZa` = 'VA') or (`slaganje`.`ZnakZa` = 'SK') or (`slaganje`.`ZnakZa` = 'ST') or (`slaganje`.`ZnakZa` = 'JA') or (`slaganje`.`ZnakZa` = 'VO') or (`slaganje`.`ZnakZa` = 'RI'))) WITH CASCADED CHECK OPTION ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `autori`
--
ALTER TABLE `autori`
  ADD PRIMARY KEY (`IdMod`,`IdClan`),
  ADD KEY `FK_Autori_IdClan` (`IdClan`);

--
-- Indexes for table `blokirani`
--
ALTER TABLE `blokirani`
  ADD PRIMARY KEY (`IdOd`,`IdZa`),
  ADD KEY `FK_IdZa` (`IdZa`);

--
-- Indexes for table `clanak`
--
ALTER TABLE `clanak`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `UQ_Id_Tip` (`Id`,`Tip`) USING BTREE,
  ADD KEY `IX_Id_IdRez` (`Id`,`IdRezervator`) USING BTREE,
  ADD KEY `FK_Clanak_IdRez` (`IdRezervator`),
  ADD KEY `IX_Id_Tip_Znak` (`Id`,`Tip`,`Znak`) USING BTREE;

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX` (`Id`,`IdAutor`,`IdMod`,`IdClan`) USING BTREE,
  ADD KEY `FK_Komentar_IdAutor` (`IdAutor`),
  ADD KEY `FK_Komentar_IdMod` (`IdMod`),
  ADD KEY `FK_Komentar_IdClan` (`IdClan`);

--
-- Indexes for table `kontakti`
--
ALTER TABLE `kontakti`
  ADD PRIMARY KEY (`IdM`,`IdZ`),
  ADD KEY `FK_IdZ` (`IdZ`);

--
-- Indexes for table `korisnik`
--
ALTER TABLE `korisnik`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `UQ_Username` (`Username`),
  ADD UNIQUE KEY `UQ_Id_Tip` (`Id`,`Tip`);

--
-- Indexes for table `mesecni`
--
ALTER TABLE `mesecni`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_Id_Tip_Znak` (`Id`,`Tip`,`Znak`) USING BTREE;

--
-- Indexes for table `moderator`
--
ALTER TABLE `moderator`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_Id_Tip` (`Id`,`Tip`) USING BTREE;

--
-- Indexes for table `regularan`
--
ALTER TABLE `regularan`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_Tip_Id` (`Tip`,`Id`) USING BTREE,
  ADD KEY `FK_Id_Tip` (`Id`,`Tip`);

--
-- Indexes for table `slaganje`
--
ALTER TABLE `slaganje`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `UQ_Znakovi` (`ZnakOd`,`ZnakZa`) USING BTREE,
  ADD KEY `IX_Id_Tip_Znak` (`Id`,`Tip`,`ZnakOd`) USING BTREE;

--
-- Indexes for table `zabrane`
--
ALTER TABLE `zabrane`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `IX_IdKor_IdMod` (`IdKor`,`IdMod`) USING BTREE,
  ADD KEY `FK_Zabrana_IdMod` (`IdMod`);

--
-- Indexes for table `zahtev`
--
ALTER TABLE `zahtev`
  ADD PRIMARY KEY (`IdOd`,`IdZa`),
  ADD KEY `FK_Zahtev_IdZa` (`IdZa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clanak`
--
ALTER TABLE `clanak`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `korisnik`
--
ALTER TABLE `korisnik`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `zabrane`
--
ALTER TABLE `zabrane`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `autori`
--
ALTER TABLE `autori`
  ADD CONSTRAINT `FK_Autori_IdClan` FOREIGN KEY (`IdClan`) REFERENCES `clanak` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Autori_IdMod` FOREIGN KEY (`IdMod`) REFERENCES `moderator` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `blokirani`
--
ALTER TABLE `blokirani`
  ADD CONSTRAINT `FK_IdOd` FOREIGN KEY (`IdOd`) REFERENCES `regularan` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_IdZa` FOREIGN KEY (`IdZa`) REFERENCES `regularan` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `clanak`
--
ALTER TABLE `clanak`
  ADD CONSTRAINT `FK_Clanak_IdRez` FOREIGN KEY (`IdRezervator`) REFERENCES `moderator` (`Id`);

--
-- Constraints for table `komentar`
--
ALTER TABLE `komentar`
  ADD CONSTRAINT `FK_Komentar_IdAutor` FOREIGN KEY (`IdAutor`) REFERENCES `regularan` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Komentar_IdClan` FOREIGN KEY (`IdClan`) REFERENCES `clanak` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Komentar_IdMod` FOREIGN KEY (`IdMod`) REFERENCES `moderator` (`Id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `kontakti`
--
ALTER TABLE `kontakti`
  ADD CONSTRAINT `FK_IdM` FOREIGN KEY (`IdM`) REFERENCES `regularan` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_IdZ` FOREIGN KEY (`IdZ`) REFERENCES `regularan` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mesecni`
--
ALTER TABLE `mesecni`
  ADD CONSTRAINT `FK_Mesecni` FOREIGN KEY (`Id`,`Tip`,`Znak`) REFERENCES `clanak` (`Id`, `Tip`, `Znak`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `moderator`
--
ALTER TABLE `moderator`
  ADD CONSTRAINT `FK_Moderator_Id_Tip` FOREIGN KEY (`Id`,`Tip`) REFERENCES `korisnik` (`Id`, `Tip`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `regularan`
--
ALTER TABLE `regularan`
  ADD CONSTRAINT `FK_Id_Tip` FOREIGN KEY (`Id`,`Tip`) REFERENCES `korisnik` (`Id`, `Tip`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `slaganje`
--
ALTER TABLE `slaganje`
  ADD CONSTRAINT `FK_Slaganje` FOREIGN KEY (`Id`,`Tip`,`ZnakOd`) REFERENCES `clanak` (`Id`, `Tip`, `Znak`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `zabrane`
--
ALTER TABLE `zabrane`
  ADD CONSTRAINT `FK_Zabrana_IdKor` FOREIGN KEY (`IdKor`) REFERENCES `regularan` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Zabrana_IdMod` FOREIGN KEY (`IdMod`) REFERENCES `moderator` (`Id`) ON DELETE CASCADE ON UPDATE SET NULL;

--
-- Constraints for table `zahtev`
--
ALTER TABLE `zahtev`
  ADD CONSTRAINT `FK_Zahtev_IdOd` FOREIGN KEY (`IdOd`) REFERENCES `regularan` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Zahtev_IdZa` FOREIGN KEY (`IdZa`) REFERENCES `regularan` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
