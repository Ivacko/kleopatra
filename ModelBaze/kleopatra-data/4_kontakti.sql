-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2017 at 11:10 AM
-- Server version: 5.7.17-log
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `kontakti`
--

CREATE TABLE `kontakti` (
  `IdM` int(11) NOT NULL,
  `IdZ` int(11) NOT NULL,
  `OcenaM` int(11) NOT NULL DEFAULT '3',
  `OcenaZ` int(11) NOT NULL DEFAULT '3',
  `DatumOd` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kontakti`
--

INSERT INTO `kontakti` (`IdM`, `IdZ`, `OcenaM`, `OcenaZ`, `DatumOd`) VALUES
(6, 19, 3, 3, '2017-06-01'),
(16, 6, 3, 3, '2017-06-15'),
(17, 6, 3, 3, '2017-06-07'),
(17, 8, 3, 3, '2017-06-15'),
(18, 1, 3, 3, '2017-06-07'),
(18, 2, 3, 3, '2017-06-09'),
(18, 3, 3, 3, '2017-06-13'),
(18, 4, 3, 3, '2017-06-01'),
(18, 8, 3, 3, '2017-06-05'),
(20, 6, 3, 3, '2017-06-08');

--
-- Triggers `kontakti`
--
DELIMITER $$
CREATE TRIGGER `decBrKontakta` BEFORE DELETE ON `kontakti` FOR EACH ROW BEGIN
	update regularan
    set BrKontakta = BrKontakta - 1
    where Id = OLD.IdM or Id = OLD.IdZ;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `incBrojKontakta` AFTER INSERT ON `kontakti` FOR EACH ROW BEGIN
	update regularan
    set BrKontakta = BrKontakta + 1
    where Id = NEW.IdM or Id = NEW.IdZ;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `updateSlaganje` AFTER UPDATE ON `kontakti` FOR EACH ROW BEGIN
    declare vZnakM char(3);
    declare vZnakZ char(3);
    select Znak into vZnakM from regularan
    where Id = new.IdM;
    select Znak into vZnakZ from regularan
    where Id = new.IdZ;
    
    if old.OcenaM <> new.OcenaM THEN
    	if old.OcenaM >= 3 then
            update slaganje
            SET OcenaPoz = OcenaPoz - (old.OcenaM - 3)
            where ZnakOd = vZnakM and ZnakZa = vZnakZ;
        else
        	update slaganje
            SET OcenaNeg = OcenaNeg - (3 - old.OcenaM)
            where ZnakOd = vZnakM and ZnakZa = vZnakZ;
        end if;
        if new.OcenaM >= 3 then
            update slaganje
            SET OcenaPoz = OcenaPoz + (new.OcenaM - 3)
            where ZnakOd = vZnakM and ZnakZa = vZnakZ;
        else
        	update slaganje
            SET OcenaNeg = OcenaNeg + (3 - new.OcenaM)
            where ZnakOd = vZnakM and ZnakZa = vZnakZ;
        end if;
    end if;
    
    if old.OcenaZ <> new.OcenaZ THEN
    	if old.OcenaZ >= 3 then
            update slaganje
            SET OcenaPoz = OcenaPoz - old.OcenaZ + 3
            where ZnakOd = vZnakZ and ZnakZa = vZnakM;
        else
        	update slaganje
            SET OcenaNeg = OcenaNeg + old.OcenaZ - 3
            where ZnakOd = vZnakZ and ZnakZa = vZnakM;
        end if;
        if new.OcenaZ >= 3 then
            update slaganje
            SET OcenaPoz = OcenaPoz + new.OcenaZ - 3
            where ZnakOd = vZnakZ and ZnakZa = vZnakM;
        else
        	update slaganje
            SET OcenaNeg = OcenaNeg - new.OcenaZ + 3
            where ZnakOd = vZnakZ and ZnakZa = vZnakM;
        end if;
    end if;

END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kontakti`
--
ALTER TABLE `kontakti`
  ADD PRIMARY KEY (`IdM`,`IdZ`),
  ADD KEY `FK_IdZ` (`IdZ`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kontakti`
--
ALTER TABLE `kontakti`
  ADD CONSTRAINT `FK_IdM` FOREIGN KEY (`IdM`) REFERENCES `regularan` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_IdZ` FOREIGN KEY (`IdZ`) REFERENCES `regularan` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
